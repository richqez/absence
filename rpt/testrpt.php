<?php
$id = $_GET['id'];
require_once('../tcpdf/tcpdf.php');

class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo

        // Set font
        $this->SetFont('angsanaupc', 'B', 18);
        // Title
        $this->SetY(15);
        $this->Cell(0, 0, 'มหาวิทยาลัยสยาม', 0, 0, 'C');
        $this->SetFont('angsanaupc', 'B', 16);
  		  $this->SetY(25);
        $this->Cell(0, 0, 'ใบลา', 0, 0, 'C');
        $this->SetY(10);
        $this->Cell(0, 0, 'ม.ศ. 201', 0, 0, 'R');


    }

     public function Footer() {



     }

}


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// remove default header/footer
$pdf->setHeaderData();
$pdf->setFooterData();
$pdf->setPrintHeader(true);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(20, 20, 20);
$pdf->SetHeaderMargin(25);
$pdf->SetFooterMargin(20);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('angsanaupc', '', 15);
$pdf->AddPage('P', 'A4');
$pdf->setPage(1, true);
$pdf->SetY(40);
// --- test backward editing ---
$con=mysqli_connect("localhost","root","","absence");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
$con->set_charset("utf8");
$result = mysqli_query($con,"SELECT * FROM absence INNER JOIN employee on absence.employee_id = employee.id INNER JOIN type_of_leave ON absence.type_of_leave_id = type_of_leave.id INNER JOIN
                      type_of_agencies on employee.type_of_agencies_id = type_of_agencies.id_agen INNER JOIN
                      branch on employee.branch_id = branch.id_b INNER JOIN faculty on employee.faculty_id = faculty.id_f
                      WHERE absence.id = $id
                      ");

while($row = mysqli_fetch_array($result))
  {
    //ใบลา
    $id2 		= (INT)$id;
    $start			= $row['start'];
    $end		= $row['end'];
    $approve_date =$row['approve_date'];
    //ตัด วันเดือนปี
    $cDateapp = explode("-",$approve_date);
    $cDatestart = explode("-",$start);
    $cDateend = explode("-",$end);
    //ตัด วันเดือนปี
    $total 			= $row['total'];
    $reason 		= $row['reason'];
    $approve_reason 		= $row['approve_reason'];
    $employee_id 			= $row['employee_id'];
    $type_of_leave_id 		= $row['type_of_leave_id'];
    $status		= $row['status'];
    $approved_by 		= $row['approved_by'];
    $docdate 		= $row['docdate'];
    //ข้อมูลคนลา
    $first_name		= $row['first_name'];
    $last_name			= $row['last_name'];
    $sex		= $row['sex'];
    $address 			= $row['address'];
    $email 		= $row['email'];
    $emp_id			= $row['emp_id'];
    //การลา
    $label			= $row['label'];
    $description			= $row['description'];
    //คณะ
    $faculty = $row['name_f'];
    $branch = $row['name_b'];

    if ($approved_by != null) {
      $nameb = mysqli_query($con,"SELECT employee.position,employee.last_name ,employee.first_name  FROM employee
                            where employee.id = $approved_by
                            ");
      $nameboss = mysqli_fetch_array($nameb);
      $bossn = $nameboss['first_name'];
      $bossl = $nameboss['last_name'];
      $bossp = $nameboss['position'];


      $cDateapp[0] += 543 ;
      switch ($cDateapp[1]) {
        case '01':$cDateapp[1]  = "มกราคม" ;break;
        case '02':$cDateapp[1]  = "กุมภาพันธ์";break;
        case '03':$cDateapp[1]  = "มีนาคม";break;
        case '04':$cDateapp[1]  = "เมษายน";break;;
        case '05':$cDateapp[1]  = "พฤษภาคม";break;
        case '06':$cDateapp[1]  = "มิถุนายน";break;
        case '07':$cDateapp[1]  = "กรกฎาคม";break;
        case '08':$cDateapp[1]  = "สิงหาคม";break;
        case '09':$cDateapp[1]  = "กันยายน";break;
        case '10':$cDateapp[1]  = "ตุลาคม";break;
        case '11':$cDateapp[1]  = "พฤศจิกายน";break;
        case '12':$cDateapp[1]  = "ธันวาคม";break;
        default:
          # code...
          break;
      }

    }

    //count
    $count1 = mysqli_query($con,"SELECT COUNT(id) AS c1 FROM absence
                            where employee_id = $employee_id
                            ");
    $rowc = mysqli_fetch_array($count1);
    $c1 = $rowc['c1'];
    $count2 = mysqli_query($con,"SELECT COUNT(id) AS c1 FROM absence
                            where employee_id = $employee_id AND id <= $id2
                            ");
    $rowc1 = mysqli_fetch_array($count2);
    $c2 = $rowc1['c1'];
    //จำนวนลากิจ
    $counts = mysqli_query($con,"SELECT SUM(total) AS totalc FROM absence
                            where employee_id = $employee_id AND type_of_leave_id = 2 AND id < $id2
                            ");
    $row2 = mysqli_fetch_array($counts);

    //จำนวนลาป่วย
    $sick = mysqli_query($con,"SELECT SUM(total) AS totals FROM absence
                            where  employee_id = $employee_id AND type_of_leave_id = 1 AND id < $id2
                            ");
    $row3 = mysqli_fetch_array($sick);

    //เงื่อนไข
    if($type_of_leave_id == 1)
    {
      if($row2['totalc'] == NULL){$totalc = "0";}else{$totalc = $row2['totalc'];}
      if($row3['totals'] == NULL){$totalsick = "0";}else{$totalsick = $row3['totals'];}
    }else
    {
      if($row2['totalc'] == NULL){$totalc = "0";}else{$totalc = $row2['totalc'];}
      if($row3['totals'] == NULL){$totalsick = "0";}else{$totalsick = $row3['totals'];}
    }

    //รวมจำนวนการลา
    $rest = mysqli_query($con,"SELECT SUM(total) AS totalr FROM absence
                          where employee_id = $employee_id AND id <= $id2
                          ");
    $row4 = mysqli_fetch_array($rest);
    $totalrest = $row4['totalr'];

    //ครั้งสุดท้ายที่ลา
    $datebe = mysqli_query($con,"SELECT start FROM absence
                          where employee_id = $employee_id AND id < $id2
                          ");
    $row5 = mysqli_fetch_array($datebe);
    $datebef = $row5['start'];
    $cDatebe = explode("-",$datebef);
    $cDatebe[0] += 543 ;
    switch ($cDatestart[1]) {
      case '01':$cDatebe[1]  = "มกราคม" ;break;
      case '02':$cDatebe[1]  = "กุมภาพันธ์";break;
      case '03':$cDatebe[1]  = "มีนาคม";break;
      case '04':$cDatebe[1]  = "เมษายน";break;;
      case '05':$cDatebe[1]  = "พฤษภาคม";break;
      case '06':$cDatebe[1]  = "มิถุนายน";break;
      case '07':$cDatebe[1]  = "กรกฎาคม";break;
      case '08':$cDatebe[1]  = "สิงหาคม";break;
      case '09':$cDatebe[1]  = "กันยายน";break;
      case '10':$cDatebe[1]  = "ตุลาคม";break;
      case '11':$cDatebe[1]  = "พฤศจิกายน";break;
      case '12':$cDatebe[1]  = "ธันวาคม";break;
      default:
        # code...
        break;
    }
    //วันที่เขียน
    if($c2 > 1)
    {
      $pdf->SetXY(75,125);
      $pdf->Cell(0, 0, ''.$cDatebe[2].'', 0, 0, 'L');
      $pdf->SetXY(100,125);
      $pdf->Cell(0, 0, ''.$cDatebe[1].'', 0, 0, 'L');
      $pdf->SetXY(135,125);
      $pdf->Cell(0, 0, ''.$cDatebe[0].'', 0, 0, 'L');

    }else {
      $cDatebe[2] ="";
      $cDatebe[1] ="";
      $cDatebe[0] ="";

    }


    $h_detail = 'ครั้งที่.........../............';
    $h_detail .= '<div align = "Right">เขียนที่มหาวิทยาลัยสยาม</div>';
    $h_detail .= '<div align = "Right">วันที่.............เดือน.......................พ.ศ...............</div><br>';
    $h_detail .= 'เรื่อง&nbsp;  '.$label.'<br>';
    $h_detail .= 'เรียน&nbsp;  อธิการบดี<br>';
    $h_detail .= '<br>';
    //บรรทัดที่ 1 เนื้อหา
    $pdf->SetXY(42,39);
    $pdf->Cell(0, 0, ''.$c1.'', 0, 0, 'L');
    $pdf->SetXY(31,39);
    $pdf->Cell(0, 0, ''.$c2.'', 0, 0, 'L');
    $pdf->SetXY(38.5,99.4);
    $pdf->Cell(0, 0, 'ข้าพเจ้า', 0, 0, 'L');
    $pdf->SetXY(60,98.6);
    $pdf->Cell(0, 0, ''.$first_name.'  '.$last_name.' ', 0, 0, 'L');
    //บรรทัดที่ 2 เนื้อหา
    $pdf->SetXY(157,105.4);
    $pdf->Cell(0, 0, ''.$total.'', 0, 0, 'L');
    $pdf->SetX(40);
    $pdf->Cell(0, 0, ''.$faculty.' สาขา '.$branch.'', 0, 0, 'L');

    //บรรทัดที่ 4 เนื้อหา
    $pdf->SetXY(40,118.5);
    $pdf->Cell(0, 0, ''.$reason.'', 0, 0, 'L');
    $pdf->SetXY(50,132);
    $pdf->Cell(0, 0, ''.$totalc.'', 0, 0, 'L');
    $pdf->SetXY(90,132);
    $pdf->Cell(0, 0, ''.$totalsick.'', 0, 0, 'L');
    $pdf->SetXY(137,132);
    $pdf->Cell(0, 0, ''.$totalrest.'', 0, 0, 'L');

    $pdf->SetXY(125,178.2);
    $pdf->Cell(0, 0, ''.$first_name.'  '.$last_name.' ', 0, 0, 'L');
    $pdf->SetXY(140,18);
    $pdf->Cell(38,13, '', 1, $ln=0, 'C', 0, '', 0, false, 'T', 'R');
    $pdf->SetXY(148,18);
    $pdf->Cell(0, 0, 'รหัสประจำตัว', 0, 0, 'L');
    $pdf->SetXY(149,23);
    $pdf->Cell(0, 0, ''.$emp_id.'', 0, 0, 'L');
    //วันที่ เดือน ปี เริ่ม
    $cDatestart[0] +=543 ;
    switch ($cDatestart[1]) {
      case '01':$cDatestart[1]  = "มกราคม" ;break;
      case '02':$cDatestart[1]  = "กุมภาพันธ์";break;
      case '03':$cDatestart[1]  = "มีนาคม";break;
      case '04':$cDatestart[1]  = "เมษายน";break;;
      case '05':$cDatestart[1]  = "พฤษภาคม";break;
      case '06':$cDatestart[1]  = "มิถุนายน";break;
      case '07':$cDatestart[1]  = "กรกฎาคม";break;
      case '08':$cDatestart[1]  = "สิงหาคม";break;
      case '09':$cDatestart[1]  = "กันยายน";break;
      case '10':$cDatestart[1]  = "ตุลาคม";break;
      case '11':$cDatestart[1]  = "พฤศจิกายน";break;
      case '12':$cDatestart[1]  = "ธันวาคม";break;
      default:
        # code...
        break;
    }
    //วันที่เขียน
    $pdf->SetXY(133,59.4);
    $pdf->Cell(0, 0, ''.$cDatestart[2].'', 0, 0, 'L');
    $pdf->SetXY(150,59.4);
    $pdf->Cell(0, 0, ''.$cDatestart[1].'', 0, 0, 'L');
    $pdf->SetXY(177,59.4);
    $pdf->Cell(0, 0, ''.$cDatestart[0].'', 0, 0, 'L');
    //วันที่เริม
    $pdf->SetXY(32,112);
    $pdf->Cell(0, 0, ''.$cDatestart[2].'', 0, 0, 'L');
    $pdf->SetXY(60,112);
    $pdf->Cell(0, 0, ''.$cDatestart[1].'', 0, 0, 'L');
    $pdf->SetXY(90,112);
    $pdf->Cell(0, 0, ''.$cDatestart[0].'', 0, 0, 'L');

    //วันที่ เดือน ปี สิ้นสุด
    switch ($cDateend[1]) {
      case '01':$cDateend[1]  = "มกราคม" ;break;
      case '02':$cDateend[1]  = "กุมภาพันธ์";break;
      case '03':$cDateend[1]  = "มีนาคม";break;
      case '04':$cDateend[1]  = "เมษายน";break;;
      case '05':$cDateend[1]  = "พฤษภาคม";break;
      case '06':$cDateend[1]  = "มิถุนายน";break;
      case '07':$cDateend[1]  = "กรกฎาคม";break;
      case '08':$cDateend[1]  = "สิงหาคม";break;
      case '09':$cDateend[1]  = "กันยายน";break;
      case '10':$cDateend[1]  = "ตุลาคม";break;
      case '11':$cDateend[1]  = "พฤศจิกายน";break;
      case '12':$cDateend[1]  = "ธันวาคม";break;
      default:
        # code...
        break;
    }
    $cDateend[0] += 543 ;
    $pdf->SetXY(118,112);
    $pdf->Cell(0, 0, ''.$cDateend[2].'', 0, 0, 'L');
    $pdf->SetXY(145,112);
    $pdf->Cell(0, 0, ''.$cDateend[1].'', 0, 0, 'L');
    $pdf->SetXY(175,112);
    $pdf->Cell(0, 0, ''.$cDateend[0].'', 0, 0, 'L');

    if ($approved_by != null) {
      if($status == "approved")
      {
        $pdf->SetXY(25,204.8);
        $pdf->Cell(0, 0, 'อนุมัติการลา', 0, 0, 'L');
        $pdf->SetXY(25,211.5);
        $pdf->Cell(0, 0, ''.$approve_reason.'', 0, 0, 'L');
        $pdf->SetXY(78,224.5);
        $pdf->Cell(0, 0, ''.$bossn.'  '.$bossl, 0, 0, 'L');
        $pdf->SetXY(78,231);
        $pdf->Cell(0, 0, ''.$bossp.'', 0, 0, 'L');
        $pdf->SetXY(64,238);
        $pdf->Cell(0, 0, ''.$cDateapp[2].'', 0, 0, 'L');
        $pdf->SetXY(76,238);
        $pdf->Cell(0, 0, ''.$cDateapp[1].'', 0, 0, 'L');
        $pdf->SetXY(94,238);
        $pdf->Cell(0, 0, ''.$cDateapp[0].'', 0, 0, 'L');
      }
      else if($status == "disapprove")
      {
        $pdf->SetXY(25,204.8);
        $pdf->Cell(0, 0, 'ไม่อนุมัติการลา', 0, 0, 'L');
        $pdf->SetXY(25,211.5);
        $pdf->Cell(0, 0, ''.$approve_reason.'', 0, 0, 'L');
        $pdf->SetXY(74,218);
        $pdf->Cell(0, 0, ''.$bossn.'  '.$bossl, 0, 0, 'L');
        $pdf->SetXY(76,224.7);
        $pdf->Cell(0, 0, ''.$bossp.'', 0, 0, 'L');
        $pdf->SetXY(64,231);
        $pdf->Cell(0, 0, ''.$cDateapp[2].'', 0, 0, 'L');
        $pdf->SetXY(76,231);
        $pdf->Cell(0, 0, ''.$cDateapp[1].'', 0, 0, 'L');
        $pdf->SetXY(94,231);
        $pdf->Cell(0, 0, ''.$cDateapp[0].'', 0, 0, 'L');
      }
      else
      {
        $pdf->SetXY(25,204.8);
        $pdf->Cell(0, 0, 'รอการอนุมัติ', 0, 0, 'L');

      }
    }

    $pdf->SetY(40);

    $detail = '<div>';
    $detail .='<span align="Right">..................................................................................................................................อาจารย์/เจ้าหน้าที่</span><br>';
    $detail .='(คณะ/แผนก)................................................................................มีความประสงค์จะขอลาหยุดงาน............วัน นับตั้งแต่วัน<br>';
    $detail .='วันที่......................เดือน.................................พ.ศ................  ถึงวันที่....................เดือน...................................พ.ศ................<br>';
    $detail .='เนื่องจาก..................................................................................................................................................................................<br>';
    $detail .='ครั้งสุดท้ายข้าพเจ้าได้ลา&nbsp;เมื่อวันที่.......................เดือน.....................................พ.ศ....................และในปีการศึกษานี้<br>';
    $detail .='ข้าพเจ้าได้ลากิจ...........................วัน ลาป่วย............................วัน รวมครั้งนี้เป็น.........................วัน';
    $detail .='</div>';
    $detail .='<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;จึงเรียนมาเพื่อโปรดพิจารณา</div>';
    $detail .='<div align="Right">ขอแสดงความนับถือ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>';
    $detail .='.....................................................&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>';
    $detail .='(........................................................)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><br><br>';



    $tb_footer = '<table border="1" width="100%" align="Right" >
    		<tbody>
    			<tr>
          	 <td align="left">
                  <span  style="text-decoration:underline;">ความเห็นผู้บังคับบัญชา<br></span>
                  <span>..............................................................................................
                  ..............................................................................................</span><br>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>ลงชื่อ.....................................</span><br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>(................................)</span><br>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>ตำแหน่ง................................</span><br>

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>............/....................../..........</span>
             </td>

             <td align="left" >
                  <span style="text-decoration:underline;">คำสั่ง<br></span>
                  <span>..............................................................................................
                  ..............................................................................................</span><br>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>ลงชื่อ.....................................</span><br>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>(................................)</span><br>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>ตำแหน่ง................................</span><br>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>............/....................../..........</span>
             </td>
          </tr>
                ';
    $tb_footer .='</table><br>';
    $f_detail ='<br>';
    $f_detail .= '&nbsp;&nbsp;<span style="text-decoration:underline;font-weight: bold;">หมายเหตุ</span>
    &nbsp;&nbsp; 1. การเสนอใบลา ต้องผ่านผู้บังคับบัญชาชั้นต้นตามสายการปฏิบัติงาน ในกรณีที่ผู้บังคับบัญชา<br>
    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    ชั้นต้นไม่อยู่ และมีความจำเป็นรีบด่วน ให้เสนอต่อผู้บังคับบัญชาในระดับถัดไป';
    $f_detail .='<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. ให้ยื่นใบลาต่อสำนักงานอธิการบดี เพื่อนำเสนออธิการบดี';
  }
// Print text using writeHTMLCell()
$pdf->writeHTML($h_detail. $detail. $tb_footer. $f_detail, true, false, false, false, '');
// ---------------------------------------------------------
//Close and output PDF document
$pdf->Output();

 ?>
