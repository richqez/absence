<?php
$y= $_GET['y'];
$m = $_GET['m'];
$mr = $_GET['mr'];
$la = $_GET['la'];
if ($_GET['faculty_n'] == "allf" || $_GET['faculty_n'] == "null") {
  $allf = $_GET['faculty_n'];
  $fac ='';
}else{
  if ($_GET['fac'] == "alls") {
   $fac ='';
  }else{
   $fac =$_GET['fac']; 
  }
  $allf =$_GET['faculty_n'];
}

require_once('../tcpdf/tcpdf.php');
$con=mysqli_connect("localhost","root","","absence");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
$con->set_charset("utf8");


class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $y2= $_GET['y'];
       
        if ($_GET['m'] == "00") {
           $m2 = "ทั้งหมด";
        }else{
          $m2 = $_GET['m'];
        }
        $mr2 = $_GET['mr'];
        $la2 = $_GET['la'];
        if ($_GET['faculty_n'] == "allf" || $_GET['faculty_n'] == "null") {
          $allf = $_GET['faculty_n'];
          $fac ='';
        }else{
          if ($_GET['fac'] == "alls") {
           $fac ='';
          }else{
           $fac =$_GET['fac']; 
          }
          $allf =$_GET['faculty_n'];
        }
        $con=mysqli_connect("localhost","root","","absence");
        if ($_GET['faculty_n'] == "allf") {
          $dbcon = mysqli_query($con,"SELECT name_b FROM branch                              
                              ");
        }else{
          if ($fac != "") {
            $dbcon = mysqli_query($con,"SELECT name_b FROM branch
                              where id_b = $fac
                              ");  
          }else if($fac == "" && $allf != ""){
            $dbcon = mysqli_query($con,"SELECT name_b FROM branch
                              where ref_faculty = $allf
                              ");
          }else{
            $dbcon = mysqli_query($con,"SELECT name_b FROM branch
                              where id_b = $fac
                              ");
          }
         
        }
        
        $row5 = mysqli_fetch_array($dbcon);

        // Set font
        $image_file = K_PATH_IMAGES.'siam.png';
        $this->SetXY(132,10);
        $this->Image($image_file);
        $this->SetFont('angsanaupc', 'B', 18);
        $idpsn = $row5['name_b'];
        $this->SetY(55);
        $this->Cell(0, 0, 'มหาวิทยาลัยสยาม', 0, 0, 'C');
        $this->SetFont('angsanaupc', 'B', 16);
        switch ($m2) {
          case '01':$m2  = "มกราคม" ;break;
          case '02':$m2  = "กุมภาพันธ์";break;
          case '03':$m2  = "มีนาคม";break;
          case '04':$m2 = "เมษายน";break;;
          case '05':$m2  = "พฤษภาคม";break;
          case '06':$m2 = "มิถุนายน";break;
          case '07':$m2 = "กรกฎาคม";break;
          case '08':$m2  = "สิงหาคม";break;
          case '09':$m2  = "กันยายน";break;
          case '10':$m2  = "ตุลาคม";break;
          case '11':$m2  = "พฤศจิกายน";break;
          case '12':$m2  = "ธันวาคม";break;
          default:
            # code...
            break;
        }
        if ($mr2 == 00) {
          $this->SetY(64);
          $this->Cell(0, 0, 'สรุปข้อมูลการลาในรอบเดือน '.$m2.' ปี พ.ศ. '.$y2.'', 0, 0, 'C');
        }else {
          switch ($mr2) {
            case '01':$mr2  = "มกราคม" ;break;
            case '02':$mr2  = "กุมภาพันธ์";break;
            case '03':$mr2  = "มีนาคม";break;
            case '04':$mr2 = "เมษายน";break;;
            case '05':$mr2  = "พฤษภาคม";break;
            case '06':$mr2 = "มิถุนายน";break;
            case '07':$mr2 = "กรกฎาคม";break;
            case '08':$mr2  = "สิงหาคม";break;
            case '09':$mr2  = "กันยายน";break;
            case '10':$mr2  = "ตุลาคม";break;
            case '11':$mr2  = "พฤศจิกายน";break;
            case '12':$mr2  = "ธันวาคม";break;
            default:
              # code...
              break;
          }
          $this->SetY(64);
          $this->Cell(0, 0, 'สรุปข้อมูลการลาระหว่างเดือน '.$m2.' ถึงเดือน '.$mr2.' ปี พ.ศ. '.$y2.' ', 0, 0, 'C');
        }


    }

     public function Footer() {

      $this->SetFont('angsanaupc', 'B', 16);
      $datenow = explode("-",date("d-m-Y"));
      $datenow[2] = $datenow[2]+ 543;
      $this->Cell(0, 0, 'วันที่ออกเอกสาร '.$datenow[0].'/'.$datenow[1].'/'.$datenow[2].'', 0, 0, 'L');
      $this->Cell(0, 0, 'หน้า '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 0, 'R');



     }

}


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// remove default header/footer
$pdf->setHeaderData();
$pdf->setFooterData();
$pdf->setPrintHeader(true);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(30, 80, 20);
$pdf->SetHeaderMargin(20);
$pdf->SetFooterMargin(20);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('angsanaupc', '', 15);
$pdf->AddPage('L', 'A4');
// $pdf->SetY(20);
// $logo ='<img src="../images/siam.png">';
// --- test backward editing ---
if($fac != "" ){
  if($la != 00 && $la != 4){
    $result = mysqli_query($con,"SELECT * FROM absence INNER JOIN employee on absence.employee_id = employee.id INNER JOIN type_of_leave ON absence.type_of_leave_id = type_of_leave.id INNER JOIN
                          type_of_agencies on employee.type_of_agencies_id = type_of_agencies.id_agen INNER JOIN
                          branch on employee.branch_id = branch.id_b INNER JOIN faculty on employee.faculty_id = faculty.id_f
                          where absence.type_of_leave_id = $la and employee.branch_id = $fac
                          ");
  }else{
    $result = mysqli_query($con,"SELECT * FROM absence INNER JOIN employee on absence.employee_id = employee.id INNER JOIN type_of_leave ON absence.type_of_leave_id = type_of_leave.id INNER JOIN
                          type_of_agencies on employee.type_of_agencies_id = type_of_agencies.id_agen INNER JOIN
                          branch on employee.branch_id = branch.id_b INNER JOIN faculty on employee.faculty_id = faculty.id_f
                          where employee.branch_id = $fac
                          ");
  }
}else if ($allf != "null" && $allf != "allf") {
    if ($la != 00 && $la != 4) {
      $result = mysqli_query($con,"SELECT * FROM absence INNER JOIN employee on absence.employee_id = employee.id INNER JOIN type_of_leave ON absence.type_of_leave_id = type_of_leave.id INNER JOIN
                          type_of_agencies on employee.type_of_agencies_id = type_of_agencies.id_agen INNER JOIN
                          branch on employee.branch_id = branch.id_b INNER JOIN faculty on employee.faculty_id = faculty.id_f
                          where absence.type_of_leave_id = $la and employee.faculty_id = $allf
                          ");
    }else{
      $result = mysqli_query($con,"SELECT * FROM absence INNER JOIN employee on absence.employee_id = employee.id INNER JOIN type_of_leave ON absence.type_of_leave_id = type_of_leave.id INNER JOIN
                          type_of_agencies on employee.type_of_agencies_id = type_of_agencies.id_agen INNER JOIN
                          branch on employee.branch_id = branch.id_b INNER JOIN faculty on employee.faculty_id = faculty.id_f
                          where  employee.faculty_id = $allf
                          ");
    }
}
else{
  if ($la != 00 && $la != 4) {
    $result = mysqli_query($con,"SELECT * FROM absence INNER JOIN employee on absence.employee_id = employee.id INNER JOIN type_of_leave ON absence.type_of_leave_id = type_of_leave.id INNER JOIN
                          type_of_agencies on employee.type_of_agencies_id = type_of_agencies.id_agen INNER JOIN
                          branch on employee.branch_id = branch.id_b INNER JOIN faculty on employee.faculty_id = faculty.id_f where absence.type_of_leave_id = $la                         
                          ");
  }else{
    $result = mysqli_query($con,"SELECT * FROM absence INNER JOIN employee on absence.employee_id = employee.id INNER JOIN type_of_leave ON absence.type_of_leave_id = type_of_leave.id INNER JOIN
                          type_of_agencies on employee.type_of_agencies_id = type_of_agencies.id_agen INNER JOIN
                          branch on employee.branch_id = branch.id_b INNER JOIN faculty on employee.faculty_id = faculty.id_f                         
                          ");
  }
  
}

$pdf->SetY(80);
$pdf->SetX(20);

$pdf->SetFont('angsanaupc', 'B', 16);
$h_detail = '<table border="1" cellpadding="2" align="center" >
                <tr>
                  <th  rowspan="2">รหัสประจำตัว</th>
                  <th rowspan="2">ชื่อ-นามสกุล</th>
                  <th width="130" rowspan="2">วันที่เริ่มการลา</th>
                  <th width="130" rowspan="2">วันที่สิ้นสุดการลา</th>
                  <th colspan="3" width="200">ประเภทการลา</th>
                  <th rowspan="2">เหตุผลการลา</th>
                  <th rowspan="2">จำนวนการลา</th>
                </tr>
                <tr>
                  <td>ลาป่วย</td>
                  <td>ลากิจ</td>
                  <td>ลาพักร้อน</td>
                </tr>
          ';
$countno = 1;
$totalday = 0;
while($row = mysqli_fetch_array($result))
  {

    //ใบลา
    $id2    = $row['id'];
    $start      = $row['start'];
    $end    = $row['end'];
    $approve_date =$row['approve_date'];
    //ตัด วันเดือนปี
    $cDateapp = explode("-",$approve_date);
    $cDatestart = explode("-",$start);
    $cDateend = explode("-",$end);
    //ตัด วันเดือนปี
    $total      = $row['total'];
    $reason     = $row['reason'];
    $approve_reason     = $row['approve_reason'];
    $employee_id      = $row['employee_id'];
    $type_of_leave_id     = $row['type_of_leave_id'];
    $status   = $row['status'];
    $approved_by    = $row['approved_by'];
    $docdate    = $row['docdate'];
    //ข้อมูลคนลา
    $first_name   = $row['first_name'];
    $last_name      = $row['last_name'];
    $sex    = $row['sex'];
    $address      = $row['address'];
    $email    = $row['email'];
    $emp_id     = $row['emp_id'];
    $position = $row['position'];
    //การลา
    $label      = $row['label'];
    $description      = $row['description'];
    //คณะ
    $faculty = $row['name_f'];
    $branch = $row['name_b'];

    $pdf->SetFont('angsanaupc', 'B', 16);
    $cDatestart[0]=+$cDatestart[0]+543;
    $cDateend[0]=+$cDateend[0]+543;
    if($mr != 00){
      if($mr >= $cDatestart[1]   && $y == $cDatestart[0]   ){

      switch ($cDatestart[1]) {
        case '01':$cDatestart[1]  = "มกราคม" ;break;
        case '02':$cDatestart[1]  = "กุมภาพันธ์";break;
        case '03':$cDatestart[1]  = "มีนาคม";break;
        case '04':$cDatestart[1]  = "เมษายน";break;;
        case '05':$cDatestart[1]  = "พฤษภาคม";break;
        case '06':$cDatestart[1]  = "มิถุนายน";break;
        case '07':$cDatestart[1]  = "กรกฎาคม";break;
        case '08':$cDatestart[1]  = "สิงหาคม";break;
        case '09':$cDatestart[1]  = "กันยายน";break;
        case '10':$cDatestart[1]  = "ตุลาคม";break;
        case '11':$cDatestart[1]  = "พฤศจิกายน";break;
        case '12':$cDatestart[1]  = "ธันวาคม";break;
        default:
          # code...
          break;
      }

      switch ($cDateend[1]) {
        case '01':$cDateend[1]  = "มกราคม" ;break;
        case '02':$cDateend[1]  = "กุมภาพันธ์";break;
        case '03':$cDateend[1]  = "มีนาคม";break;
        case '04':$cDateend[1]  = "เมษายน";break;;
        case '05':$cDateend[1]  = "พฤษภาคม";break;
        case '06':$cDateend[1]  = "มิถุนายน";break;
        case '07':$cDateend[1]  = "กรกฎาคม";break;
        case '08':$cDateend[1]  = "สิงหาคม";break;
        case '09':$cDateend[1]  = "กันยายน";break;
        case '10':$cDateend[1]  = "ตุลาคม";break;
        case '11':$cDateend[1]  = "พฤศจิกายน";break;
        case '12':$cDateend[1]  = "ธันวาคม";break;
        default:
          # code...
          break;
      }

      $h_detail .='
                  <tr>
                    <td>'.$emp_id.'</td>
                    <td align="left">'.$first_name.' '.$last_name.'</td>
                    <td>'.$cDatestart[2].'/'.$cDatestart[1].'/'.$cDatestart[0].'</td>
                    <td>'.$cDateend[2].'/'.$cDateend[1].'/'.$cDateend[0].'</td>';
    if ($type_of_leave_id == 1) {
      $h_detail .='
                  <td><img width="20px" height="20px" src="../images/check.png"></td>
                  <td></td>
                  <td></td>
                  ';
    }elseif ($type_of_leave_id==2) {
      $h_detail .='
                  <td></td>
                  <td><img width="20px" height="20px" src="../images/check.png"></td>
                  <td></td>
                  ';
    }elseif ($type_of_leave_id==3) {
      $h_detail .='
                  <td></td>
                  <td></td>
                  <td><img width="20px" height="20px" src="../images/check.png"></td>
                  ';
    }
      $h_detail .=' <td>'.$reason.'</td>
                    <td>'.$total.'</td>
                  </tr>
                  ';

      $countno++;
      $totalday = $totalday+$total;
      }
    }else if($mr == 0){
      if($m == $cDatestart[1] && $y == $cDatestart[0]  ){

      switch ($cDatestart[1]) {
        case '01':$cDatestart[1]  = "มกราคม" ;break;
        case '02':$cDatestart[1]  = "กุมภาพันธ์";break;
        case '03':$cDatestart[1]  = "มีนาคม";break;
        case '04':$cDatestart[1]  = "เมษายน";break;;
        case '05':$cDatestart[1]  = "พฤษภาคม";break;
        case '06':$cDatestart[1]  = "มิถุนายน";break;
        case '07':$cDatestart[1]  = "กรกฎาคม";break;
        case '08':$cDatestart[1]  = "สิงหาคม";break;
        case '09':$cDatestart[1]  = "กันยายน";break;
        case '10':$cDatestart[1]  = "ตุลาคม";break;
        case '11':$cDatestart[1]  = "พฤศจิกายน";break;
        case '12':$cDatestart[1]  = "ธันวาคม";break;
        default:
          # code...
          break;
      }

      switch ($cDateend[1]) {
        case '01':$cDateend[1]  = "มกราคม" ;break;
        case '02':$cDateend[1]  = "กุมภาพันธ์";break;
        case '03':$cDateend[1]  = "มีนาคม";break;
        case '04':$cDateend[1]  = "เมษายน";break;;
        case '05':$cDateend[1]  = "พฤษภาคม";break;
        case '06':$cDateend[1]  = "มิถุนายน";break;
        case '07':$cDateend[1]  = "กรกฎาคม";break;
        case '08':$cDateend[1]  = "สิงหาคม";break;
        case '09':$cDateend[1]  = "กันยายน";break;
        case '10':$cDateend[1]  = "ตุลาคม";break;
        case '11':$cDateend[1]  = "พฤศจิกายน";break;
        case '12':$cDateend[1]  = "ธันวาคม";break;
        default:
          # code...
          break;
      }

      $h_detail .='
                  <tr>
                    <td>'.$emp_id.'</td>
                    <td align="left">'.$first_name.' '.$last_name.'</td>
                    <td>'.$cDatestart[2].'/'.$cDatestart[1].'/'.$cDatestart[0].'</td>
                    <td>'.$cDateend[2].'/'.$cDateend[1].'/'.$cDateend[0].'</td>';
    if ($type_of_leave_id == 1) {
      $h_detail .='
                  <td><img width="20px" height="20px" src="../images/check.png"></td>
                  <td></td>
                  <td></td>
                  ';
    }elseif ($type_of_leave_id==2) {
      $h_detail .='
                  <td></td>
                  <td><img width="20px" height="20px" src="../images/check.png"></td>
                  <td></td>
                  ';
    }elseif ($type_of_leave_id==3) {
      $h_detail .='
                  <td></td>
                  <td></td>
                  <td><img width="20px" height="20px" src="../images/check.png"></td>
                  ';
    }
      $h_detail .=' <td>'.$reason.'</td>
                    <td>'.$total.'</td>
                  </tr>
                  ';

      $countno++;
      $totalday = $totalday+$total;
    }
  }
}

  switch ($m) {
    case '00':$m  = "ทั้งหมด" ;break;
    case '01':$m  = "มกราคม" ;break;
    case '02':$m  = "กุมภาพันธ์";break;
    case '03':$m  = "มีนาคม";break;
    case '04':$m = "เมษายน";break;;
    case '05':$m  = "พฤษภาคม";break;
    case '06':$m = "มิถุนายน";break;
    case '07':$m = "กรกฎาคม";break;
    case '08':$m  = "สิงหาคม";break;
    case '09':$m  = "กันยายน";break;
    case '10':$m  = "ตุลาคม";break;
    case '11':$m  = "พฤศจิกายน";break;
    case '12':$m  = "ธันวาคม";break;
    default:
      # code...
      break;
  }
$h_detail .='
              <tr>
                <td colspan="8" align="center">รวมการลาทั้งหมดในรอบเดือน '.$m.'</td>
                <td>'.$totalday.'</td>
              </tr>
              ';
$h_detail .='  </table>';



// Print text using writeHTMLCell()
$pdf->writeHTML($h_detail, true, false, false, false, '');
// ---------------------------------------------------------
//Close and output PDF document
$pdf->Output();

 ?>