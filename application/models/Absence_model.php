<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Absence_model extends CI_Model {

	public $id;
	public $start;
	public $end;
	public $total ;
	public $reason ;
	public $employee_id ;
	public $type_of_leave_id ;
	public $docdate ;
	public $att_file ;

	public function __construct()
	{
		parent::__construct();
	}

	public function findAll(){
		return $this->db->get('type_of_leave')->result();
	}
	public function countRest1(){
		// $this->db->get_where('absence',array('employee_id'=>$userObj[0]->id,'type_of_leave_id'=>1 ))->num_rows();
		$userObj  = $this->session->userdata('userLogin');
		$this->db->select_sum('absence.total');
		$this->db->where(array('employee_id'=>$userObj[0]->id,'type_of_leave_id'=>1 ));

		return $this->db->get('absence')->result();
	}
	public function countRest2(){
		$userObj  = $this->session->userdata('userLogin');
		$this->db->select_sum('absence.total');
		$this->db->where(array('employee_id'=>$userObj[0]->id,'type_of_leave_id'=>2 ));

		return $this->db->get('absence')->result();
	}
	public function countRest3(){
		$userObj  = $this->session->userdata('userLogin');
		$this->db->select_sum('absence.total');
		$this->db->where(array('employee_id'=>$userObj[0]->id,'type_of_leave_id'=>3 ));

		return $this->db->get('absence')->result();
	}
	public function Notifications(){
			$userObj  = $this->session->userdata('userLogin');
		return $this->db->get_where('absence',array('status'=>'wait'))->num_rows();
	}
	public function countByUser(){
		$userObj  = $this->session->userdata('userLogin');
		return $this->db->get_where('absence',array('employee_id'=>$userObj[0]->id ))->num_rows();
	}

	public function findForList($start,$limit,$typeid){
		$userObj  = $this->session->userdata('userLogin');
		$this->db->select('absence.id as a_id , absence.* , type_of_leave.label');
		//$this->db->get_where('absence',array('employee_id'=>$userObj[0]->id ),$limit,$start)
		$this->db->join('type_of_leave', 'type_of_leave.id = absence.type_of_leave_id');
		if ($typeid!=null and $typeid!= 'all') {
				$this->db->where(array('employee_id'=>$userObj[0]->id,'type_of_leave_id'=>$typeid));
		}
		else if ($typeid == null || $typeid == 'all') {
			$this->db->where(array('employee_id'=>$userObj[0]->id));
		}
		$query = $this->db->get('absence',$limit,$start);
		//var_dump($query->result());
		return $query->result();

	}

	public function findForAppoved(){

		$this->db->select('absence.id as a_id , absence.* , type_of_leave.label');
		$this->db->join('type_of_leave', 'type_of_leave.id = absence.type_of_leave_id');
		$this->db->order_by("id", "desc");
		$this->db->where(array('status' => 'wait'));
		$query = $this->db->get('absence');

		return $query->result();
	}

	public function countAll(){
		return $this->db->get('absence')->num_rows();
	}

	public function insert(){




		$userObj  = $this->session->userdata('userLogin');

		$dirName = 'att_file/';
	  $temp = explode(".", $_FILES["file"]["name"]);
	  $newfilename = $dirName . round(microtime(true)) . '.' . end($temp) ;
	  move_uploaded_file($_FILES['file']['tmp_name'],$newfilename);

		if($_FILES['file']['tmp_name'] != null){

			$this->start = formatDateToSave($this->input->post('start'));
			$this->end = formatDateToSave($this->input->post('end'));
			$this->total = getDateRange($this->start,$this->end);
			$this->reason = $this->input->post('reason');
			$this->type_of_leave_id = $this->input->post('type');
			$this->docdate = getdate();
			$this->employee_id = $userObj[0]->id;
			$this->att_file = $newfilename;


		}else {
			$this->start = formatDateToSave($this->input->post('start'));
			$this->end = formatDateToSave($this->input->post('end'));
			$this->total = getDateRange($this->start,$this->end);
			$this->reason = $this->input->post('reason');
			$this->type_of_leave_id = $this->input->post('type');
			$this->docdate = getdate();
			$this->employee_id = $userObj[0]->id;
			$this->att_file = "noattfile";
		}






		$this->db->insert('absence',$this);
	}

	public function appoved($id){
		$this->db->update('absence',array('status'=>'approved'),array('id' => $id));
	}

	public function unappoved($id){
		$this->db->update('absence',array('status'=>'wait'),array('id' => $id ));
	}
	public function update(){

	}
	public function delete($id){
		$this->db->delete('absence',array('id'=>$id));
		//r_dump($id);
	}

	public function agency(){
		$userObj  = $this->session->userdata('userLogin');
		$this->db->select('*');
		$this->db->join('branch', 'branch.ref_faculty = faculty.id_f');
		$query = $this->db->get('faculty');
		return $query->result();
	}



}

/* End of file Absence_Model.php */
/* Location: ./application/models/Absence_Model.php */
