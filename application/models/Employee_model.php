<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_model extends CI_Model {

	public $id;
	public $user_name;
	public $password;
	public $first_name;
	public $last_name;
	public $sex;
	public $address ;
	public $email;
	public $branch_id;
	public $faculty_id;
	public $position;
	public $role;
	public $emp_id;



	public function __construct(){
		parent::__construct();
		$this->load->helper('file');

	}

	public function findByUser(){

		return $this->db->get_where("employee",
		array("user_name"=> $this->input->post('username') ,
		"password"=>  $this->input->post('password') ))->result();

	}

	public function findById($id){
		return $this->db->get_where("employee",array('id'=>$id))->result();
	}

	public function showuser()
	{

		$this->db->from('employee');
		$this->db->join('type_of_agencies', 'employee.type_of_agencies_id = type_of_agencies.id_agen');
		$this->db->join('faculty', 'employee.faculty_id = faculty.id_f');
		$this->db->join('branch', 'employee.branch_id = branch.id_b');
		$query = $this->db->get();
		return $query->result();
	}

	public function add(){

		$this->user_name =  $this->input->post('username');
		$this->password =  $this->input->post('password');
		$this->first_name = $this->input->post('firstname');
		$this->last_name =  $this->input->post('lastname');
		$this->sex =  $this->input->post('sex');
		$this->position = $this->input->post('position');
		$this->address = $this->input->post('addr');
		$this->email = $this->input->post('email');
		$this->type_of_agencies_id = 1;
		$this->role = $this->input->post('role');
		$this->emp_id = $this->input->post('emp_id');
		$this->branch_id = $this->input->post('branch_n');
		$this->faculty_id = $this->input->post('faculty_n');

		$this->db->insert('employee',$this);

	}

	public function update(){

		$this->id =$this->input->post('id');
		$this->user_name =  $this->input->post('username');
		$this->password =  $this->input->post('password');
		$this->first_name = $this->input->post('firstname');
		$this->last_name =  $this->input->post('lastname');
		$this->sex =  $this->input->post('sex');
		$this->emp_id =  $this->input->post('emp');
		$this->position = $this->input->post('position');
		$this->address = $this->input->post('addr');
		$this->email = $this->input->post('email');
		$this->role = $this->input->post('role');
		$this->branch_id = $this->input->post('branch_n');
		$this->type_of_agencies_id = 1;
		$this->faculty_id = $this->input->post('faculty_n');



		$dirName = 'im/';
	  $temp = explode(".", $_FILES["file"]["name"]);
	  $newfilename = $dirName . round(microtime(true)) . '.' . end($temp) ;
	  move_uploaded_file($_FILES['file']['tmp_name'],$newfilename);

		if($_FILES['file']['tmp_name'] != null){
			$updateDate = array('password' => $this->input->post('password'),
			'first_name' => $this->input->post('firstname'),
			'last_name' => $this->input->post('lastname'),
			'position' => $this->position = $this->input->post('position') ,
			'address'=>$this->input->post('addr'),
			'email'=>$this->input->post('email'),
			'emp_id'=>$this->input->post('emp'),
			'branch_id' =>$this->input->post('branch_n'),
			'faculty_id' =>$this->input->post('faculty_n'),
			'sex'=>$this->input->post('sex'),
			'img_path'=> $newfilename);
		}else {
			$updateDate = array('password' => $this->input->post('password'),
			'first_name' => $this->input->post('firstname'),
			'last_name' => $this->input->post('lastname'),
			'position' => $this->position = $this->input->post('position') ,
			'address'=>$this->input->post('addr'),
			'emp_id'=>$this->input->post('emp'),
			'email'=>$this->input->post('email'),
			'branch_id' =>$this->input->post('branch_n'),
			'faculty_id' =>$this->input->post('faculty_n'),
			'sex'=>$this->input->post('sex'));
		}



		$id = $this->input->post('id');

		$this->db->where('id',$this->input->post('id'));
		$this->db->update('employee',$updateDate);


	}

	public function delete($id){

		$this->db->delete('employee', array('id' => $id));

	}

	public function edituser($id){
		$this->db->from('employee');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result();
	}







}

/* End of file employee.php */
/* Location: ./application/models/employee.php */
