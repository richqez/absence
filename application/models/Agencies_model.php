<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agencies_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function findAll(){
    return $this->db->get('type_of_agencies')->result();
  }
  public function facultyAll(){
    return $this->db->get('faculty')->result();
  }
  public function branchAll(){
    return $this->db->get('branch')->result();
  }

}
