
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม</title>

  <?php include_once 'scriptandcss.php'; ?>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
  <?php $userObj  = $this->session->userdata('userLogin'); ?>
  <?php include_once 'submenu.php'; ?>
  <!-- Start Logo Section -->

  <!-- Start Main Body Section -->
  <div class="mainbody-section text-center">
    <div class="container">
      <div class="row">

          <div class="col-md-6" style="margin-top:10%;margin-left:22%">
            <!-- Start Carousel Section -->

            <!-- Start Carousel Section -->

            <div class="row">
              <div class="col-md-6">

                  <div class="menu-item color responsive">
                    <a href="<?php echo base_url() ?>RptCtrl/rptpsnal">
                      <i class="fa fa-file-pdf-o"></i>
                      <p>สุรปรายงานการลารายบุคคล</p>
                    </a>
                  </div>

              </div>

              <div class="col-md-6">
                <!--  menu for boss-->
                <!-- show for boss only -->

                  <div class="menu-item light-orange responsive-2">
                      <a href="<?php echo base_url() ?>RptCtrl">
                        <i class="fa fa-file-pdf-o">

                        </i>
                      <p>สรุปรายงานการลารายเดือน</p>
                    </a>
                  </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </body>

  </html>
