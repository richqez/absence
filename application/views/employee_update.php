<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-5">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม</title>

	<?php include_once 'scriptandcss.php'; ?>

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript">

	</script>
	<style type="text/css">
	#Rtxt{
		text-align: right;
		font-family: sans-serif;
	}
	p{
		font-size: 20px;
	}
	h3{
		text-decoration: underline;
	}

	</style>
</head>
<body>
<?php include_once 'submenu.php'; ?>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="page-header">
					<h2>แก้ไขข้อมูลส่วนตัว </h2>

				</div>

			</div>
				</div>
				<div class="row">
					<div class="panel panel-default" style="margin-top:5%">
						<br>
						<!-- end menuuser -->
						<div class="panel-body" style="margin-bottom:5%" >
							<div class="container">
								<?php echo form_open_multipart('EmployeeController/update'); ?>

								<div style="text-align:center">
									<img src="<?php echo base_url(). $employee->img_path?>" width="100" height="100" class="img-circle" >
								</div>
								<br/>
								<div class="row">
									<div class="col-lg-3" id="Rtxt">
										<p>ชื่อผู้ใช้</p>
									</div>
									<div class="col-lg-5">
										<input type="text" name="username"  disabled="true" class="form-control" value="<?php echo $employee->user_name; ?>" />
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-lg-3" id="Rtxt">
										<p>รหัสผู้ใช้</p>
									</div>
									<div class="col-lg-5">
										<input type="password" name="password" value="<?php echo $employee->password; ?>" class="form-control" />
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-lg-3" id="Rtxt">
										<p>รหัสประจำตัว</p>
									</div>
									<div class="col-lg-5">
										<input type="text" name="emp"  class="form-control" value="<?php echo $employee->emp_id; ?>">
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-lg-3" id="Rtxt">
										<p>ชื่อ</p>
									</div>
									<div class="col-lg-5">
										<input type="text" name="firstname"  class="form-control" value="<?php echo $employee->first_name; ?>">
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-lg-3" id="Rtxt">
										<p>นามสกุล</p>
									</div>
									<div class="col-lg-5">
										<input type="text" name="lastname" class="form-control" value="<?php echo $employee->last_name; ?>">
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-lg-3" id="Rtxt">
										<p>ตำแหน่ง</p>
									</div>
									<div class="col-lg-5">
										<input type="text" name="position" class="form-control" value="<?php echo $employee->position; ?>">
									</div>
								</div>
								<!-- <br> -->
								<!-- agent -->
								<!-- <div class="row">
								<div class="col-lg-3" id="Rtxt">
								<p>แผนก</p>
							</div>
							<div class="col-lg-5">
							<select class="form-control" name="agent">
							<?php foreach ($agencies as $key => $value): ?>
							<option value="<?php echo $value->id ?>"
							<?php if ($value->id == $employee->type_of_agencies_id): ?>
							<?php echo 'selected="selected"' ?>
						<?php endif; ?>
						><?php echo $value->name; ?>
					</option>
				<?php endforeach; ?>
			</select>

		</div>
	</div> -->

	<br>
	<div class="row">
		<div class="col-lg-3" id="Rtxt">
			<p>คณะ</p>
		</div>
		<div class="col-lg-5">
			<select class="form-control" name="faculty_n" id="faculty">
				<?php foreach ($faculty as $key => $value): ?>
					<option value="<?php echo $value->id_f ?>"
						<?php if ($value->id_f == $employee->faculty_id): ?>
							<?php echo 'selected="selected"' ?>
						<?php endif; ?>
						>
						<?php echo $value->name_f; ?>
					</option>
				<?php endforeach; ?>
			</select>

		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-lg-3" id="Rtxt">
			<p>สาขา</p>
		</div>
		<div class="col-lg-5">
			<select class="form-control" name="branch_n">
				<?php foreach ($branch as $key => $value): ?>
					<option value="<?php echo $value->id_b ?>"
						<?php if ($value->id_b == $employee->branch_id): ?>
							<?php echo 'selected="selected"' ?>
						<?php endif; ?>
						><?php echo $value->name_b; ?>
					</option>
				<?php endforeach; ?>
			</select>

		</div>
	</div>
	<br>
	<!-- Sex -->
	<div class="row">
		<div class="col-lg-3" id="Rtxt">
			<p>เพศ</p>
		</div>
		<div class="col-lg-5">
			<label>
				<input type="radio" name="sex"  value="m"
				<?php if ($employee->sex == 'm'): ?>
					<?php echo 'checked' ?>
				<?php endif; ?> />
				<span>ชาย</span>
			</label>
			<label>
				<input type="radio" name="sex"  value="f"
				<?php if ($employee->sex == 'f'): ?>
					<?php echo 'checked' ?>
				<?php endif; ?>
				/>
				<span>หญิง</span>
			</label>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-lg-3" id="Rtxt" multiline="3">
			<p>ที่อยู่</p>
		</div>
		<div class="col-lg-5">
			<textarea name="addr" class="form-control" rows="5" ><?php echo $employee->address; ?></textarea>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-lg-3" id="Rtxt">
			<p>อีเมล์</p>
		</div>
		<div class="col-lg-5">
			<input type="text" name="email" class="form-control" value="<?php echo $employee->email; ?>">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-lg-3" id="Rtxt">
			<p>รูป</p>
		</div>
		<div class="col-lg-5">
			<input type="file" name="file" >
		</div>
	</div>
	<div class="row">
		<div class="col-lg-3" id="Rtxt">
			<input type="hidden" name="id" value="<?php echo $employee->id ?>" />
			<input type="hidden" name="role" value="<?php echo $employee->role ?>" />
			<input type="hidden" name="type_of_agencies_id" value="<?php echo $employee->type_of_agencies_id ?>" />
		</div>
		<div class="col-lg-5">
			<input type="submit" class="btn btn-default" value="ตกลง">
			<?php echo form_close(); ?>
			<a class="btn btn-default" href="<?php echo base_url() ?>homecontroller/login" >ยกเลิก</a>
		</div>
	</div>

</div>
<a class="btn btn-info" href="<?php echo base_url() . 'homeController' ?>" role="button"><span class="glyphicon glyphicon-circle-arrow-left"></span> กลับหน้าหลัก</a>
</div>

</div>
</div>
</div>



<!-- Javascript -->


<script type="text/javascript">


$('#faculty').change(function(){
	var facultyId = this.value;
	if(facultyId!='null'){
		$.ajax({
			method:'POST',
			url:'MenuUserController/getBranch',
			data:{ faculty_id : facultyId }
		})
		.done(function(response){
			var html = '';
			$.each(response.data,function(i,item){
				html += '<option value="'+item.id_b+'">'+item.name_b+'</option>';
			});
			$('select[name="branch_n"]').html(html);
			$('select[name="branch_n"]').prop('disabled',false);
		})
	}
	else {
		$('select[name="branch_n"]').prop('disabled',true);
		$('select[name="branch_n"]').html('');
	}
})
</script>

</body>
</html>
