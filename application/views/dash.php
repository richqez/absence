
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม</title>

  <?php include_once 'scriptandcss.php'; ?>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
  <?php $userObj  = $this->session->userdata('userLogin'); ?>
  <?php include_once 'submenu.php'; ?>
  <!-- Start Logo Section -->
  <section id="logo-section" class="text-center">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="logo text-center">
            <h1><img src="<?php echo base_url(); ?>/img/logo.png"/></h1>
            <span><strong>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม</strong></span>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Logo Section -->


  <!-- Start Main Body Section -->
  <div class="mainbody-section text-center">
    <div class="container">
      <div class="row">

        <div class="col-md-3">

          <div class="menu-item blue">
            <?php $userObj  = $this->session->userdata('userLogin'); ?>
            <a href="" data-toggle="modal">
              <img src="<?php echo base_url(). $userObj[0]->img_path?>" width="100" height="100" class="img-circle" >
              <p>
                <?php echo $userObj[0]->first_name; ?>
              </p>
              <p>

              </p>

            </div>

            <div class="menu-item green">
              <a href="<?php echo base_url().'EmployeeController?id='.$userObj[0]->id ?>" data-toggle="modal">
                <i class="fa fa-user"></i>
                <p>Profile ข้อมูลส่วนตัว</p>
              </a>
            </div>

               <div class="menu-item light-red">
                 <a href="<?php echo base_url() ?>AbsenceController/create" data-toggle="modal">
                   <i class="fa fa-file-text-o"></i>
                   <p>Absence Form แบบฟอร์มการลา</p>
                 </a>
               </div>

          </div>
          <div class="col-md-6">
            <!-- Start Carousel Section -->
            <div class="home-slider">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="padding-bottom: 30px;">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="<?php echo base_url(); ?>/images/about-03.jpg" class="img-responsive" alt="">
                  </div>
                  <div class="item">
                    <img src="<?php echo base_url(); ?>/images/about-02.jpg" class="img-responsive" alt="">
                  </div>
                  <div class="item">
                    <img src="<?php echo base_url(); ?>/images/about-01.jpg" class="img-responsive" alt="">
                  </div>

                </div>

              </div>
            </div>
            <!-- Start Carousel Section -->

            <div class="row">
              <div class="col-md-6">

                  <div class="menu-item color responsive">
                    <a href="<?php echo base_url() ?>AbsenceCtrl?withuser=yes">
                      <i class="fa fa-history"></i>
                      <p>Absence History ประวัติการลา</p>
                    </a>
                  </div>

              </div>

              <div class="col-md-6">
                <!--  menu for boss-->
                <!-- show for boss only -->
                <?php if ($userObj[0]->role == 'boss'): ?>
                  <div class="menu-item light-orange responsive-2">
                      <a href="<?php echo base_url() ?>AbsenceCtrl?withuser=no">
                        <i class="fa fa-check-square-o">

                        </i>
                      <p>Absence Approve อนุมัติการลา</p>
                    </a>
                  </div>
                
                <?php endif; ?>
              </div>

            </div>

          </div>

          <div class="col-md-3">

            <div class="menu-item light-red">
              <a href="<?php echo base_url();?>HomeController/logout" >
                <i class="fa fa-power-off"></i>
                <p>Logout ออกจากระบบ</p>
              </a>
            </div>
            <!--  show for admin only  -->
            <?php if ($userObj[0]->role == 'admin'): ?>
              <div class="menu-item color">
                <a href="<?php echo base_url();?>AbsenceController/mainsys" >
                  <i class="fa fa-plus-circle"></i>
                  <p>การจัดการ</p>
                </a>
              </div>
            <?php endif; ?>

            <?php if ($userObj[0]->role == 'admin'): ?>
            <div class="menu-item blue">
              <a href="<?php echo base_url() ?>RptCtrl/rpt" data-toggle="modal">
                <i class="fa fa-file-pdf-o"></i>
                <p>ออกรายงาน</p>
              </a>
            </div>
            <?php endif; ?>

          </div>
        </div>
      </div>
    </div>
    <!-- End Main Body Section -->

    <!-- Start Copyright Section -->
    <div class="copyright text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div>Copyright &copy; 2015 <a href="http://www.siam.edu" target="_blank">Siam University</a>. All right reserved.</a></div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Copyright Section -->



  </body>

  </html>
