<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม</title>

	<?php include_once 'scriptandcss.php'; ?>



	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>
	<?php include_once 'submenu.php'; ?>
	<div class="container">
		<div class="row">
			<div class="panel panel-default" style="margin-top:2%">
				<div class="panel-heading">แบบฟร์อมใบลา</div>
				<div class="panel-body">

					<?php echo 	form_open_multipart('AbsenceController/save',array('class'=>'form-horizontal')); ?>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">ประเภทการลา</label>
						<div class="col-sm-4">
							<select class="form-control" name="type">
								<?php foreach ($selectType as $key ): ?>
									<option value="<?php echo $key->id; ?>"><?php echo $key->label; ?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>

					<div class="form-group">

					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">วันที่เริ่มต้นการลา</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="start" name="start"  placeholder="" required="true">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">วันที่สิ้นสุดการลา</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="end" name="end"  placeholder="" required="true">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">เหตุผล</label>
						<div class="col-sm-6">
							<textarea class="form-control" rows="10" name="reason" required="true"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">เอกสารแนบ</label>
						<div class="col-sm-6">
							<input type="file" name="file" >
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-default">บันทึก</button>
							<a class="btn btn-default" href="<?php echo base_url() ?>homecontroller/login" >ยกเลิก</a>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>


<!--script src="../js/bootstrap-datepicker-thai.js" charset="utf-8"></script-->

<script type="text/javascript">
$('#start')
	.datepicker({
		language:'th',
		format:'dd-mm-yyyy',
		todayHighlight :true,
		todayBtn:true,
		orientation:'bottom',
		autoclose:true
}).on('changeDate',function(){
	var start  = $('#start').datepicker('getDate','+1d');
	start.setDate(start.getDate()+1);
	$('#end').datepicker('remove');
	$('#end')
		.datepicker({
			language:'th',
			startDate : start,
			format:'dd-mm-yyyy',
			orientation:'bottom',
			autoclose:true,
		});
	$('#end').datepicker('setDate',start);
});
$('#end')
	.datepicker({
		language:'th',
		startDate :"today",
		format:'dd-mm-yyyy',
		orientation:'bottom',
		autoclose:true,
	}).on('changeDate',function(){

			$.ajax({
				method:'POST',
				async:false,
				url :'../AbsenceController/checkDate',
				data: { start: $('#start').val() , end: $('#end').val() , type : $('select[name="type"]').val() }
			})
			.done(function(response){

				if(!response.status){
					alert("คุณลาเกินจำนวนวันวันลาสูงสุดไป"+response.xdata+"วัน");
					}


			});


	});
</script>
