<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-5">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม</title>

	<?php include_once 'scriptandcss.php'; ?>

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script type="text/javascript">

	</script>
	<style type="text/css">
	.b_arrow{
		border-radius: 10px;

		margin-top: 20px;
		margin-left: auto;
		margin-right: auto;
		opacity: 0.7;

	}
	#Rtxt{
		text-align: right;
		font-family: sans-serif;
	}
	p{
		font-size: 20px;
	}
	h3{
		text-decoration: underline;
	}
	</style>
</head>
<body>
<?php include_once 'submenu.php'; ?>
	<div class="container">
		<div class="row">
			<div class="col-md-6">


			</div>
		</div>
		<div class="row">
			<div class="panel panel-default" style="margin-top:5%">
				<!-- menuuser -->
				<div class="panel-body">
				<div class="page-header">

						<h1>เพิ่มผู้ใช้งานในระบบการลา </h1>
						<small>เพื่มชื่อผู้ใช้</small>

				</div>
				<br>
				<!-- end menuuser -->
				<div class="panel-body" style="margin-bottom:5%" >
					<input type="hidden" id="hdid" >
					<div class="container">
						<?php echo form_open('MenuUserController/adduser'); ?>
						<h3>เพิ่มผู้ใช้ในระบบ</h3>
						<br>

						<div class="row">
							<div class="col-lg-3" id="Rtxt">
								<p>รหัสประจำตัว</p>
							</div>
							<div class="col-lg-5">
								<input type="text" name="emp_id" require="true"  class="form-control" required>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-lg-3" id="Rtxt">
								<p>คณะ</p>
							</div>
							<div class="col-lg-5">
								<select class="form-control" name="faculty_n" id="faculty">

									<option value="null" >---โปรดเลือกคณะ---</option>
									<?php foreach ($faculty as  $value): ?>
										<option value="<?php echo $value->id_f;?> "><?php echo $value->name_f; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-3" id="Rtxt">
								<p>สาขา</p>
							</div>
							<div class="col-lg-5">
								<select class="form-control" name="branch_n" disabled="true">

								</select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-3" id="Rtxt">
								<p>ชื่อผู้ใช้</p>
							</div>
							<div class="col-lg-5">
								<input type="text" name="username" class="form-control" required>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-lg-3" id="Rtxt">
								<p>รหัสผู้ใช้</p>
							</div>
							<div class="col-lg-5">
								<input type="text" name="password" class="form-control" required>
							</div>
						</div>

						<br/><br>
						<div class="row">
							<div class="col-lg-3" id="Rtxt">
								<p>ชื่อ</p>
							</div>
							<div class="col-lg-5">
								<input type="text" name="firstname" class="form-control" required>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-3" id="Rtxt">
								<p>นามสกุล</p>
							</div>
							<div class="col-lg-5">
								<input type="text" name="lastname" class="form-control" required>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-3" id="Rtxt">
								<p>ตำแหน่ง</p>
							</div>
							<div class="col-lg-2">
								<select class="form-control" name="position">
									<option value="อธิการบดี">อธิการบดี</option>
									<option value="รองอธิการบดี">รองอธิการบดี</option>
									<option value="คณบดีบัณฑิตวิทยาลัย">คณบดีบัณฑิตวิทยาลัย</option>
									<option value="ผู้ช่วยคณบดีบัณฑิตวิทยาลัย">ผู้ช่วยคณบดีบัณฑิตวิทยาลัย</option>
									<option value="ผู้อำนวยการ">ผู้อำนวยการ</option>
									<option value="รองผู้อำนวยการ">รองผู้อำนวยการ</option>
									<option value="อาจารย์">อาจารย์</option>
									<option value="นักศึกษา">นักศึกษา</option>
									<option value="บุคลากร">บุคลากร</option>
								</select>
							</div>
						</div>
						<br>

						<!-- Sex -->
						<div class="row">
							<div class="col-lg-3" id="Rtxt">
								<p>เพศ</p>
							</div>
							<div class="col-lg-5">
								<label>
									<input type="radio" name="sex"  value="m" checked="true">
									<span>ชาย</span>
								</label>
								<label>
									<input type="radio" name="sex"  value="f">
									<span>หญิง</span>
								</label>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-3" id="Rtxt" multiline="3">
								<p>ที่อยู่</p>
							</div>
							<div class="col-lg-5">
								<textarea name="addr" class="form-control" rows="5"></textarea>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-3" id="Rtxt">
								<p>อีเมล์</p>
							</div>
							<div class="col-lg-5">
								<input type="email" name="email" class="form-control">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-3" id="Rtxt">
								<p>สถานะ</p>
							</div>
							<div class="col-lg-2">
								<select class="form-control" name="role">
									<option value="user">บุคลากร</option>
									<option value="admin">ผู้ดูแลระบบ</option>
									<option value="boss">หัวหน้า</option>
								</select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-3" id="Rtxt">

							</div>
							<div class="col-lg-5">
								<input type="submit" class="btn btn-default" value="ตกลง">
								<a class="btn btn-default" href="<?php echo base_url() ?>homecontroller/login" >ยกเลิก</a>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
						<a class="btn btn-info" style="margin-top:20px;margin-bottom:-20px" href="<?php echo base_url() . 'homeController' ?>" role="button"><span class="glyphicon glyphicon-circle-arrow-left"></span> กลับหน้าหลัก</a>

					</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Javascript -->
	<script src="../js/bootstrap-datepicker.js" charset="utf-8"></script>
	<script src="../js/bootstrap-datepicker.th.js" charset="utf-8"></script>
	<script type="text/javascript">
		$('#faculty').change(function(){
			var facultyId = this.value;
			if(facultyId!='null'){
				$.ajax({
					method:'POST',
					url:'../MenuUserController/getBranch',
					data:{ faculty_id : facultyId }
				})
				.done(function(response){
					var html = '';
					$.each(response.data,function(i,item){
						html += '<option value="'+item.id_b+'">'+item.name_b+'</option>';
					});
					$('select[name="branch_n"]').html(html);
					$('select[name="branch_n"]').prop('disabled',false);
				})
			}
			else {
				$('select[name="branch_n"]').prop('disabled',true);
				$('select[name="branch_n"]').html('');
			}
		})
	</script>
</body>
</html>
