<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-5">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม</title>

	<?php include_once 'scriptandcss.php'; ?>

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript">

	</script>
	<style type="text/css">
	.b_arrow{
		border-radius: 10px;

		margin-top: 150px;
		margin-left: 10px;
		margin-right: 10px;
		margin-bottom: 10px;
		opacity: 0.7;

	}
	#Rtxt{
		text-align: right;
		font-family: sans-serif;
	}
	p{
		font-size: 20px;
	}
	h3{
		text-decoration: underline;
	}
	</style>
</head>
<body>
	<?php include_once 'submenu.php'; ?>
	<div class="container">
		<div class="row">
			<div class="panel panel-default" style="margin-top:5%">
				<div class="panel-body">
				<div class="page-header">

						<h1>ตารางแสดงรายชื่อผู้ใช้ทั้งหมดของระบบ </h1>
						<small>รายชื่อผู้ใช้</small>

				</div>
				<br>
				<!-- end menuuser -->
				<div class="table-responsive">
					<table class="table" >
						<tr >
							<th>
								#
							</th>
							<th>รหัสประจำตัว</th>
							<th>ชื่อ</th>
							<th>นามสกุล</th>
							<th>เพศ</th>
							<th>ที่อยู่</th>
							<th>อีเมล์</th>
							<th>สาขา</th>
							<th>คณะ</th>
							<th>ตำแหน่ง</th>
							<th>สถานะ</th>
							<th>ดำเนินการ</th>
						</tr>

						<?php foreach ($query as $key => $row ) : ?>
							<tr>
								<td>
									<?php echo $key+1 ?>
								</td>
								<td ><?php echo $row->emp_id; ?></td>
								<td ><?php echo $row->first_name; ?></td>
								<td ><?php echo $row->last_name; ?></td>
								<td ><?php if($row->sex == "m"){echo "ชาย";}else{echo "หญิง";} ?></td>
								<td ><?php echo $row->address; ?></td>
								<td ><?php echo $row->email; ?></td>
								<td ><?php echo $row->name_b; ?></td>
								<td ><?php echo $row->name_f; ?></td>
								<td ><?php echo $row->position; ?></td>
								<td ><?php echo $row->role; ?></td>
								<td><?php echo anchor('MenuUserController/del/'.$row->id, '<i class="glyphicon glyphicon-remove"></i>', $row->id); ?>
									<?php echo  anchor('EmployeeController?id='.$row->id, '<i class="glyphicon glyphicon-pencil"></i>', $row->id); ?></td>
								</tr>
							<?php endforeach; ?>


						</table>
					</div>
					<a class="btn btn-info" style="margin-top:200px;margin-left:10px;margin-bottom:10px" href="<?php echo base_url() . 'homeController' ?>" role="button"><span class="glyphicon glyphicon-circle-arrow-left"></span> กลับหน้าหลัก</a>
					</div>
					</div>
  	</div>
				</div>
			</div>
		</div>



		<!-- Javascript -->
		<script src="../js/bootstrap-datepicker.js" charset="utf-8"></script>
		<script src="../js/bootstrap-datepicker.th.js" charset="utf-8"></script>
	</body>
	</html>
