<?php $userObj  = $this->session->userdata('userLogin');
?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม <?php echo date("Y-m-d") ?></title>

  <?php include_once 'scriptandcss.php'; ?>
  <style media="screen">
  .autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
  .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
  .autocomplete-selected { background: #F0F0F0; }
  .autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
  .autocomplete-group { padding: 2px 5px; }
  .autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
  </style>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
  #div_ #sp_right{float:right}
  </style>
</head>
<body >
  <?php include_once 'submenu.php'; ?>

  <div class="container">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">

            <div class="page-header">
              <?php if ($userObj[0]->role != 'boss' || $this->session->userdata('withuser') == 'yes'): ?>
                <h1>แสดงรายงานรายบุคคล </h1>
                <small>กรุณาใส่รหัสประจำตัว เลือกเดือนและปีในการออกรายงาน</small>
              <?php endif; ?>
            </div>

          </div>

          <div class="col-md-6" align="right" style="margin-top:3%" >

          </div>
        </div>
        <form action="/absence/rpt/personalrptnew.php" method="GET">
        <div class="row" id="rootControl">
          <div class="form-horizontal">

            <!-- <div class="form-group">
              <label class="col-sm-2 control-label">การค้นหา</label>
              <div class="col-sm-3">
                <input class="form-control" type="text" id="autocomplete" name="full" >
              </div>

            </div> -->
            <div class="form-group">
              <label class="col-sm-2 control-label">ชื่อ-นามสกุล</label>
              <div class="col-sm-4">
                <input class="form-control" type="text" id="autocomplete"   name="student_name"   >
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">รหัสประจำตัว</label>
              <div class="col-sm-4">
                <input class="form-control" type="text" id="autocomplete2"  name="student_id"  >
              </div>
            </div>
            <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">วันที่เริ่มต้นการลา</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="start" name="start"  placeholder="" required="true">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">วันที่สิ้นสุดการลา</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="end" name="end"  placeholder="" required="true">
            </div>
          </div>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 "></label>
          <div class="col-sm-4">
            <button id="btn1" type="submit" class="btn btn-warning">แสดงรายงาน</button>
            <!-- <a href="../absence/rpt/testrpt.php?id=m" class="btn btn-info">ดาวโหลดเอกสาร PDF</a> -->
          </div>
          </form>
          <br>
          <br>
          <a class="btn btn-info" style="margin-top:20px;margin-bottom:-20px" href="<?php echo base_url() . 'homeController' ?>" role="button"><span class="glyphicon glyphicon-circle-arrow-left"></span> กลับหน้าหลัก</a>

        </div>


      </div>

    </div>
  </div>



  <script src="../js/bootstrap-datepicker.js" charset="utf-8"></script>
  <script src="../js/bootstrap-datepicker.th.js" charset="utf-8"></script>
  <!--script type="text/javascript">
    $('#faculty').change(function(){
      var facultyId = this.value;
      if(facultyId!='null'){
        $.ajax({
          method:'POST',
          url:'../MenuUserController/getBranch',
          data:{ faculty_id : facultyId }
        })
        .done(function(response){
          var html = '';
          $.each(response.data,function(i,item){
            html += '<option value="'+item.id_b+'">'+item.name_b+'</option>';
          });
          $('select[name="fac"]').html(html);
          $('select[name="fac"]').prop('disabled',false);
        })
      }
      else {
        $('select[name="fac"]').prop('disabled',true);
        $('select[name="fac"]').html('');
      }
    })
  </script-->
  <!--script type="text/javascript">
  $('#m').change(function(){
    var m = this.value;
    if(m =='00'){



        $('select[name="mr"]').html('');
        $('select[name="mr"]').prop('disabled',true);

    }
    else {
        var html = '';
      html += '<option value="00">--เลือกเดือน--</option>';
      html += '<option value="01">มกราคม</option>';
      html += '<option value="02">กุมภาพันธ์</option>';
      html += '<option value="03">มีนาคม</option>';
      html += '<option value="04">เมษายน</option>';
      html += '<option value="05">พฤษภาคม</option>';
      html += '<option value="06">มิถุนายน</option>';
      html += '<option value="07">กรกฎาคม</option>';;
      html += '<option value="08">สิงหาคม</option>';
      html += '<option value="09">กันยายน</option>';
      html += '<option value="10">ตุลาคม</option>';
      html += '<option value="11">พฤศจิกายน</option>';
      html += '<option value="12">ธันวาคม</option>';
        // <option value="01">มกราคม</option>
        // <option value="02">กุมภาพันธ์</option>
        // <option value="03">มีนาคม</option>
        // <option value="04">เมษายน</option>
        // <option value="05">พฤษภาคม</option>
        // <option value="06">มิถุนายน</option>
        // <option value="07">กรกฎาคม</option>
        // <option value="08">สิงหาคม</option>
        // <option value="09">กันยายน</option>
        // <option value="10">ตุลาคม</option>
        // <option value="11">พฤศจิกายน</option>
        // <option value="12">ธันวาคม</option>';
      $('select[name="mr"]').prop('disabled',false);
      $('select[name="mr"]').html(html);
    }
  })
  </script-->
</body>
</html>
<script type="text/javascript">
  $(document).ready(function(){
  // $('#btn1').click(function(e){

  //     var clickEvent = e ;


  //     if (!(parseInt($('#day').val()) >= 1  && parseInt($('#day').val()) <= 31)) {
  //       alert('วันห้ามเกิน 31');
  //       clickEvent.preventDefault();
  //       $('#day').focus()

  //     }
  //     if (!(parseInt($('#day2').val()) >= 1  && parseInt($('#day2').val()) <= 31)) {
  //       alert('วันห้ามเกิน 31');
  //       clickEvent.preventDefault();
  //       $('#day').focus()

  //     }
  //     if($('#autocomplete').val() == "" && $('#autocomplete2').val() == ""){
  //         alert('กรุณาใส่ชื่อ-นามสกุล หรือ รหัสประจำตัว');
  //         clickEvent.preventDefault();
  //         $('#autocomplete').focus()
  //     }
  //     if($('select[name="faculty_n"]').val() == "null"){
  //         alert('กรุณาเลือกคณะ');
  //         clickEvent.preventDefault();
  //         $('select[name="faculty_n"]').focus()
  //     }


  // })

    $('#autocomplete').autocomplete({
      serviceUrl: '../RptCtrl/valid?keyword='+$('#autocomplete').val(),
      onSelect: function (suggestions) {
      //  $('#autocomplete2').val(suggestion.data);
      var all= suggestions.value.split(",");
      // console.log(all[0]);

      }
    });
    $('#autocomplete2').autocomplete({
      serviceUrl: '../RptCtrl/valid2?keyword='+$('#autocomplete2').val(),
      onSelect: function (suggestions) {
      //  $('#autocomplete2').val(suggestion.data);
      var all= suggestions.value.split(",");
      // console.log(all[0]);

      }
    });




  })
</script>
<script type="text/javascript">
$('#start')
  .datepicker({
    language:'th',
    format:'dd-mm-yyyy',
    todayHighlight :true,
    todayBtn:true,
    orientation:'bottom',
    autoclose:true
}).on('changeDate',function(){
  var start  = $('#start').datepicker('getDate','+1d');
   start.setDate(start.getDate()+1);
  $('#end').datepicker('remove');
  $('#end')
    .datepicker({
      language:'th',
      startDate : start,
      format:'dd-mm-yyyy',
      orientation:'bottom',
      autoclose:true,
    });
  $('#end').datepicker('setDate',start);
});
$('#end')
  .datepicker({
    language:'th',
    startDate :"today",
    format:'dd-mm-yyyy',
    orientation:'bottom',
    autoclose:true,
  }).on('changeDate',function(){

      $.ajax({
        method:'POST',
        async:false,
        url :'../AbsenceController/checkDate',
        data: { start: $('#start').val() , end: $('#end').val() , type : $('select[name="type"]').val() }
      })
      .done(function(response){

        // if(!response.status){
        //   alert("คุณลาเกินจำนวนวันวันลาสูงสุดไป"+response.xdata+"วัน");
        //   }


      });


  });
</script>
