<?php $userObj  = $this->session->userdata('userLogin');
?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม <?php echo date("Y-m-d") ?></title>

  <?php include_once 'scriptandcss.php'; ?>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
  #div_ #sp_right{float:right}
  </style>
</head>
<body >
  <?php include_once 'submenu.php'; ?>

  <div class="container">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">

            <div class="page-header">
              <?php if ($userObj[0]->role != 'boss' || $this->session->userdata('withuser') == 'yes'): ?>
                <h1>ตารางแสดงใบลาทั้งหมดของคุณ </h1>
                <small>ประวัติการลาของคุณ</small>
              <?php else: ?>
                <h1>ตารางแสดงใบลาทั้งหมดในระบบ</h1>
                <small>ส่วนการอนุมัติใบลา</small>
              <?php endif; ?>
            </div>

          </div>

          <div class="col-md-6" align="right" style="margin-top:3%" >

          </div>
        </div>
        <div class="row">
          <?php if ($this->session->userdata('withuser') == 'yes'): ?>
            <div class="col-md-4 col-md-offset-4" id="stat">
              <ul class="list-group">
                <li class="list-group-item">
                  <span class="badge"><?php echo $rest1[0]->total ? $rest1[0]->total  : '0' ?>  วัน</span>
                  ลาป่วย
                </li>
                <li class="list-group-item">
                  <span class="badge"><?php echo $rest2[0]->total ? $rest2[0]->total  : '0' ?>  วัน</span>
                  ลากิจ
                </li>
                <li class="list-group-item">
                  <span class="badge"><?php echo $rest3[0]->total ? $rest3[0]->total  : '0' ?>  วัน</span>
                  ลาพักร้อน
                </li>
              </ul>
            </div>
          <?php endif; ?>
        </div>
        <div class="row" id="rootControl">
          <div class="form-horizontal">
            <div class="form-group">
              <label class="col-sm-2 control-label">ค้นหาจากวันที่</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" id="searchDate">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">ค้นหาจากประเภท</label>
              <div class="col-sm-4">
                <select id="searchType">
                  <option value="">--เลือกประเภท--</option>
                  <?php foreach ($type as $key => $value): ?>
                    <option value="<?php echo $value->label ?>"><?php echo $value->label ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">ค้นหาจากคณะ</label>
              <div class="col-sm-4">
                <select id="searchB">
                  <option value="">--เลือกคณะ--</option>
                  <?php foreach ($branch as $key => $value): ?>
                    <option value="<?php echo $value->name_b ?>"><?php echo $value->name_b ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <table id="absence_table" class="table" >
              <thead>
                <tr>
                  <th>#</th>
                  <th>สถาณะ</th>
                  <th>ประเภทการลา</th>
                  <th>วันที่เริ่มต้นการลา</th>
                  <th>วันที่สิ้นสุดการลา</th>
                  <th>จำนวนวันที่ลา</th>
                  <th>เหตุผลในการลาและอื่นๆ</th>
                  <th>

                  </th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">รายละเอียดการลา</h4>
        </div>
        <div class="modal-body" id="modal-body" style="text-align:center">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="myModal2" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        ...
      </div>
    </div>
  </div>

</body>
</html>

<script type="text/javascript">
var role = '<?php echo $userObj[0]->role ?>';
var userid = '<?php echo $userObj[0]->id ?>';
var withUser = '<?php echo $this->session->userdata('withuser') ?>';

if(role=='user'){
  $('#rootControl').hide();
}

if(withUser=='yes'){
  $('#rootControl').hide();
}


</script>
<script type="text/javascript">
$(document).ready(function(){



  var tableObj =$('#absence_table').DataTable({
    ajax : "AbsenceCtrl/dataTable",
    pageLength : 10,
    columnDefs : [
      {
        "targets": [ 7 ],
        "visible": false
      }

    ],
    columns:[
      {data: null},
      {mRender: function(data,type,full){
        switch (full.status) {
          case 'disapprove':
          return '<span class="label label-danger">ไม่อนุมัติ</span>' ;
          break;
          case 'approved' :
          return '<span class="label label-success">อนุมัติ</span>' ;
          break;
          case 'wait' :
          return '<span class="label label-warning">รอการอนุมัติ</span>' ;
          break;
          default:

        }
      }},
      {data: "label"},
      {data: "start"},
      {data: "end"},
      {data: "total"},{
        mRender: function(data,type,full){
          return '<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" model-btn data-target="#myModal"'+
          ' data-attfile="'+full.att_file+'"'+
          ' data-reason="'+full.reason+'"'+
          ' data-itemid="'+full.id+'"'+
          ' data-status="'+full.status+'"' +
          ' data-approved_by="'+full.approved_by+'"'+
          ' data-empname="'+full.first_name+'"' +
          ' data-nameb="'+full.name_b+'"' +
          '>กดปุ่มรายละเอียดเพิ่มเติม'+
          '</button>';

        }
      },
      {data:"name_b"}
    ],
    language: {
      "lengthMenu": "แสดง _MENU_ รายการ ต่อ หน้า",
      "zeroRecords": "ไม่พบ - ขอภัย",
      "info": "กำลังแสดงpage _PAGE_ จาก _PAGES_",
      "infoEmpty": "ไม่พบผลลัพธ์....",
      "infoFiltered": "(กรอง จาก _MAX_ รายการ)",
      "search":         "ค้นหา:",
      "paginate": {
        "first":      "หน้าแรก",
        "last":       "หน้าสุดท้าย",
        "next":       "ถัดไป",
        "previous":   "ก่อนหน้า"
      }
    }
  });

  tableObj.on( 'order.dt search.dt', function () {
    tableObj.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
      cell.innerHTML = i+1;
    } );
  } ).draw();

  $('#absence_table').on('click','button',function(){

    var data =$(this).data();

    var status =$(this).parent().parent().find('span').parent().html();
    var type = $(this).parent().parent().find('span').parent().next().html()
    var start = $(this).parent().parent().find('span').parent().next().next().html()
    var end = $(this).parent().parent().find('span').parent().next().next().next().html()
    var total = $(this).parent().parent().find('span').parent().next().next().next().next().html()
    var  xhtml = '';
    var file = '' ;
    var pdf = '';
    var appby = '';
    if(data.approved_by){
      appby ='<tr><td>ผู้อนุมัติ</td><td>'+data.approved_by+'</td></tr>';
    }

    var html =  '<table class="table" style="background-color: beige;">'+
    '<tr><td>ชื่อเจ้าของใบลา</td>'+
    '<td>'+data.empname+'</td></tr>'+
    '<tr><td>สาขา</td>'+
    '<td>'+data.nameb+'</td></tr>'+

    '<tr><td>ประเภทการลา</td>'+
    '<td>'+type+'</td>'+
    '</tr><tr><td>วันที่เริ่มต้น</td >'+
    '<td>'+start+'</td>'+
    '</tr><tr><td>วันที่สิ้นสุด</td>'+
    '<td>'+end+'</td>'+
    '</tr><tr><td>จำนวนวันที่ลา</td>'+
    '<td>'+total+'</td>'+
    '</tr><tr><td>เหตุผล</td>'+
    '<td>'+data.reason+'</td>'+
    '</tr>'+ appby +
    '</table>'+
    '<div style="font-size:20px;">สถานะ  '+status+'</div>'+
    '<br/>'+
    '<br/>';



    pdf = '<a href="../absence/rpt/testrpt.php?id='+data.itemid+'" class="btn btn-info">ดาวโหลดเอกสาร PDF</a>';

    if(data.attfile!= 'noattfile' ){
      file = '<br/>'+
      '<br/>'+
      '<a href="../absence/'+data.attfile+'" class="btn btn-info">ดาวโหลดน์เอกสารแนบ</a>';
    }
    $.ajax({
      method: 'GET',
      async:false,
      url: 'homeController/getRole'
    })
    .done(function(response){
      if (response=="no") {
        switch (data.status) {
          case 'disapprove':
          break;
          case 'approved':
          /*
          html = '<br/>'+
          '<br/>'+
          '<a href="../absence/AbsenceController/unappoved?id='+data.itemid+'" class="btn btn-primary btn-lg active">ยกเลิกการอนุมัติ</a>'+html;
          */
          break;
          case 'wait':
          xhtml = '<br/><br/>'+

          '<label class="checkbox-inline" style="    font-size: 30px;"><input type="radio"  name="adminaction"   value="approved"> อนุมัติ</label>'+
          '<label class="checkbox-inline" style="    font-size: 30px;"><input type="radio"     name="adminaction"  value="disapproved"> ไม่อนุมัติ</label> <br/><br/>'+
          '<textarea id="reasonxx" class="form-control" rows="3" placeholder="ความเห็นผู้บังคับบัญชา"></textarea><br/>'+
          '<button btnaction class="btn btn-default btn-lg" data-key="'+data.itemid+'">ตกลง</button>'
          //'<a href="../absence/AbsenceController/approved?id='+data.itemid+'"  actionbtn data-type="approved" data-itemkey="'+data.itemid+'"  class="btn btn-success btn-lg active" style="     margin-bottom: 30px"><spna class="glyphicon glyphicon-thumbs-up"></span>  กดเพื่ออนุมัติรายการนี้</a>'+
          //'<a href="../absence/AbsenceController/disapproved?id='+data.itemid+'" actionbtn="disapproved" data-type="disapproved"  data-itemkey="'+data.itemid+'" class="btn btn-danger btn-lg active" style="    margin-bottom: 30px;margin-left: 75px;"><spna class="glyphicon glyphicon-thumbs-down"></span>  กดเพื่อไม่อนุมัติรายการนี้</a><br/>';
          break;
          default:
        }

        html +=xhtml;


      }
      html+=file;
      html+= '</br></br>'+pdf;


      $('#modal-body').html(html);
      $('#reasonxx').hide();
    });



  });

  $('#modal-body').on('click','input[type="radio"]',function(){
    $('#reasonxx').show('slow');
    $('#reasonxx').focus();
  });

  $('#searchDate')
  .datepicker({
    language:'th',
    format:'dd-mm-yyyy',
    todayHighlight :true,
    todayBtn:true,
    orientation:'bottom',
    autoclose:true
  }).on('change',function(){
    tableObj.search($('#searchDate').val()).draw();
  });

  $('#searchType')
  .on('change',function(){
    tableObj.search($('#searchType').val()).draw();
  });

  $('#searchB')
  .on('change',function(){
    tableObj.search($('#searchB').val()).draw();
  });

  $.ajax({
    method: 'GET',
    url: 'homeController/getRole'
  })
  .done(function(response){
    if (response=="no") {
      alert("ระบบจะแสดงใบลาที่อยู่ในสถาณะ รอการอนุมัติ ท่านสามารถปรับเปลี่ยนเงื่อนไขการแสดงผลที่ได้ที่ ช่องค้นหาด้านขวามือ ");
      tableObj.search("รอการอนุมัติ").draw();
    }
  });






  $('#myModal').on('click','button',function(e){

    var element = $(this);
    var key = $(this).data('key') ;



    if (key != undefined) {
      var value = $('input[name="adminaction"]:checked').val();
      if(value != undefined){
        if(value == 'disapproved' ){
          if ($('#reasonxx').val().length > 5 && $('#reasonxx').val().length < 80 ) {
            $.post("../absence/AbsenceController/disapproved",{
              id : key,
              appby: userid,
              reason : $('#reasonxx').val()
            },function(data, status){
              window.location = '../absence/AbsenceCtrl?withuser=no' ;
            });
          }else{
            alert("กรุณากรอกเหตุผลในช่อง ความเห็นผู้บังคับบัญชา และ ไม่สามารถ ใส่เกิน 80 ตัวอักษร");
              $('#reasonxx').focus();
          }
        }else{
          if ($('#reasonxx').val().length < 80){
            $.post("../absence/AbsenceController/approved",{
              id : key,
              appby: userid,
              reason : $('#reasonxx').val()
            },function(data, status){
              window.location = '../absence/AbsenceCtrl?withuser=no' ;
            });

          }else {
            alert("ความเห็นผู้บังคับบัญชา ไม่สามารถ ใส่เกิน 80 ตัวอักษร");
            $('#reasonxx').focus();
          }
        }

      }else{
        alert("กรุณาดำเนินการใหม่อีกครั้ง");
      }

      /*
      if( type == 'disapproved'){

      if ($('#reasonxx').val().length > 5 && $('#reasonxx').val().length < 80 ) {

      $.post("../absence/AbsenceController/disapproved",{
      id : key,
      appby: userid,
      reason : $('#reasonxx').val()
    },function(data, status){
    window.location = '../absence/AbsenceCtrl?withuser=no' ;
  });

}else{
alert("กรุณากรอกเหตุผลในช่อง ความเห็นผู้บังคับบัญชา และ ไม่สามารถ ใส่เกิน 80 ตัวอักษร");
$('#reasonxx').focus();
}
}else {

if ($('#reasonxx').val().length < 80) {

$.post("../absence/AbsenceController/approved",{
id : key,
appby: userid,
reason : $('#reasonxx').val()
},function(data, status){
window.location = '../absence/AbsenceCtrl?withuser=no' ;
});
}
else{
alert("ความเห็นผู้บังคับบัญชา ไม่สามารถ ใส่เกิน 80 ตัวอักษร");
$('#reasonxx').focus();
}


}

*/

}
});




});
</script>
