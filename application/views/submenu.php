<?php $userObj  = $this->session->userdata('userLogin'); ?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url() ?>HomeController/login">

        <img align="left" src="<?php echo base_url(). $userObj[0]->img_path?>" width="30" height="30" class="img-circle" >
          <?php echo $userObj[0]->first_name; ?>



      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">

        <li><a href="<?php echo base_url() ?>AbsenceController/create">เขียนใบลา</a></li>
        <li><a href="<?php echo base_url() ?>AbsenceCtrl?withuser=yes">ประวัติการลา</a></li>
        <?php if ($userObj[0]->role == 'boss'): ?>
          <li><a href="<?php echo base_url();?>AbsenceCtrl?withuser=no">อนุมัติการลา<?php if($countntf != "0"){echo '('. $countntf .')';}  ?></a></li>
        <?php endif; ?>
        <?php if ($userObj[0]->role == 'admin'): ?>
          <li><a href="<?php echo base_url();?>MenuUserController/index">รายชื่อผู้ใช้งาน</a></li>
          <li><a href="<?php echo base_url() ?>MenuUserController/showadd">เพิ่มผู้ใช้งาน</a></li>
          <li><a href="<?php echo base_url() ?>DayOffCtrl">จัดการวันหยุด</a></li>
          <li><a href="<?php echo base_url() ?>RptCtrl/rpt">ออกรายงาน</a></li>

        <?php endif; ?>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><?php if ($userObj[0]->role == 'boss'): ?>
          <a href="<?php echo base_url();?>AbsenceCtrl?withuser=no" class="btn btn-warning btn-lg" style="height:44px">
            <span class="glyphicon glyphicon-bell" aria-hidden="true"></span><sup style="color:black;"><?php if($countntf != "0"){echo $countntf;}  ?></sup>
          </a>
        <?php endif; ?></li>
        <li><a href="<?php echo base_url().'EmployeeController?id='.$userObj[0]->id ?>">แก้ไขข้อมูลส่วนตัว</a></li>
        <li><a href="<?php echo base_url();?>HomeController/logout">ออกจากระบบ</a></li>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
