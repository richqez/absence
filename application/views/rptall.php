<?php $userObj  = $this->session->userdata('userLogin');
?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม <?php echo date("Y-m-d") ?></title>

  <?php include_once 'scriptandcss.php'; ?>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
  #div_ #sp_right{float:right}
  </style>
</head>
<body >
  <?php include_once 'submenu.php'; ?>

  <div class="container">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">

            <div class="page-header">
              <?php if ($userObj[0]->role != 'boss' || $this->session->userdata('withuser') == 'yes'): ?>
                <h1>แสดงรายงานรายเดือน </h1>
                <small>กรุณาเลือกเดือนและปีในการออกรายงาน</small>
              <?php endif; ?>
            </div>

          </div>

          <div class="col-md-6" align="right" style="margin-top:3%" >

          </div>
        </div>
        <form action="../absence/rpt/allpersonal.php" method="GET">
        <div class="row" id="rootControl">
          <div class="form-horizontal">

            <div class="form-group">
              <label class="col-sm-2 control-label">คณะ</label>
              <div class="col-sm-3">
                <select class="form-control" name="faculty_n" id="faculty">

									<option value="null" >---โปรดเลือกคณะ---</option>
									<?php foreach ($faculty as  $value): ?>
										<option value="<?php echo $value->id_f;?> "><?php echo $value->name_f; ?></option>
									<?php endforeach; ?>
                  <option value="allf" >เลือกทั้งหมด</option>
								</select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">สาขา</label>
              <div class="col-sm-3">
                <select class="form-control" name="fac" disabled="true">

								</select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">ประเภทการลา</label>
              <div class="col-sm-3">
                <select class="form-control"  name="la">
                  <option value="00">--เลือกประเภทการลา--</option>
                    <option value="1">ลาป่วย</option>
                    <option value="2">ลากิจ</option>
                    <option value="3">ลาพักร้อน</option>
                    <option value="4">ลาทั้งหมด</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">ตั้งแต่เดือน</label>
              <div class="col-sm-3">
                <select class="form-control"  name="m">
                  <option value="00">--เลือกเดือน--</option>
                    <option value="01">มกราคม</option>
                    <option value="02">กุมภาพันธ์</option>
                    <option value="03">มีนาคม</option>
                    <option value="04">เมษายน</option>
                    <option value="05">พฤษภาคม</option>
                    <option value="06">มิถุนายน</option>
                    <option value="07">กรกฎาคม</option>
                    <option value="08">สิงหาคม</option>
                    <option value="09">กันยายน</option>
                    <option value="10">ตุลาคม</option>
                    <option value="11">พฤศจิกายน</option>
                    <option value="12">ธันวาคม</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">ถึงเดือน</label>
              <div class="col-sm-3">
                <select class="form-control"  name="mr">
                  <option value="00">--เลือกเดือน--</option>
                    <option value="01">มกราคม</option>
                    <option value="02">กุมภาพันธ์</option>
                    <option value="03">มีนาคม</option>
                    <option value="04">เมษายน</option>
                    <option value="05">พฤษภาคม</option>
                    <option value="06">มิถุนายน</option>
                    <option value="07">กรกฎาคม</option>
                    <option value="08">สิงหาคม</option>
                    <option value="09">กันยายน</option>
                    <option value="10">ตุลาคม</option>
                    <option value="11">พฤศจิกายน</option>
                    <option value="12">ธันวาคม</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">ปี</label>
              <div class="col-sm-3">
                <select class="form-control"  name="y">
                  <option value="00">--เลือกปี--</option>
                    <option value="2558">2558</option>
                    <option value="2559">2559</option>
                    <option value="2560">2560</option>
                    <option value="2561">2561</option>
                    <option value="2562">2562</option>
                    <option value="2563">2563</option>
                    <option value="2564">2564</option>
                    <option value="2565">2565</option>
                    <option value="2566">2566</option>
                </select>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 "></label>
          <div class="col-sm-4">
            <br>
            <button type="submit" class="btn btn-warning">แสดงรายงาน</button>
            <!-- <a href="../absence/rpt/testrpt.php?id=m" class="btn btn-info">ดาวโหลดเอกสาร PDF</a> -->
          </div>
          </form>
          <br>
          <br>
          <br>
          <br>
          <a class="btn btn-info" style="margin-top:20px;margin-bottom:-20px" href="<?php echo base_url() . 'homeController' ?>" role="button"><span class="glyphicon glyphicon-circle-arrow-left"></span> กลับหน้าหลัก</a>

        </div>


      </div>
    </div>
  </div>

  <script src="../js/bootstrap-datepicker.js" charset="utf-8"></script>
  <script src="../js/bootstrap-datepicker.th.js" charset="utf-8"></script>
  <script type="text/javascript">
		$('#faculty').change(function(){
			var facultyId = this.value;
			if(facultyId!='null'){
        if(facultyId=='allf'){
          $('select[name="fac"]').prop('disabled',true);
          $('select[name="fac"]').html('');
        }else{
          $.ajax({
          method:'POST',
          url:'..MenuUserController/getBranch',
          data:{ faculty_id : facultyId }
        })
        .done(function(response){
          var html = '';
          $.each(response.data,function(i,item){

            html += '<option value="'+item.id_b+'">'+item.name_b+'</option>';
           
          });
           html += '<option value="alls">เลือกทั้งหมด</option>';
          $('select[name="fac"]').html(html);
          $('select[name="fac"]').prop('disabled',false);
        })
        }
				
			}     
			else {
				$('select[name="fac"]').prop('disabled',true);
				$('select[name="fac"]').html('');
			}
		})
	</script>

</body>

</html>
