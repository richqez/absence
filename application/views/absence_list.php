<!DOCTYPE html>
<?php $userObj  = $this->session->userdata('userLogin');
?>
<head>
	<html>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม</title>

	<?php include_once 'scriptandcss.php'; ?>

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
	#div_ #sp_right{float:right}
	</style>
</head>

<body>
	<div class="container-fuid">
		<div class="row">
			<div class="col-md-6">

				<div class="page-header">
					<?php if ($userObj[0]->role != 'boss'): ?>
						<h1>ตารางแสดงใบลาทั้งหมดของคุณ </h1>
						<small>ประวัติการลาของคุณ</small>
					<?php else: ?>
						<h1>ตารางแสดงใบลาทั้งหมดในระบบ</h1>
						<small>ส่วนการอนุมัติใบลา</small>
					<?php endif; ?>
				</div>

			</div>
			<div class="col-md-6" align="right" style="margin-top:3%" >
				<!-- Split button -->
				<?php $userObj  = $this->session->userdata('userLogin'); ?>
				<?php if ($userObj[0]->role == 'boss'): ?>
					<a href="<?php echo base_url();?>AbsenceController" class="btn btn-warning btn-lg" style="height:44px">
						<span class="glyphicon glyphicon-bell" aria-hidden="true"></span><sup style="color:black;"><?php if($countntf != "0"){echo $countntf;}  ?></sup>
					</a>
				<?php endif; ?>
				<div class="btn-group" >
					<button type="button" style="width:200px;"  class="btn btn-default"><img align="left" src="<?php echo base_url(). $userObj[0]->img_path?>" width="30" height="30" class="img-circle" >
						<span style="margin-right:15%;font-weight: bold;"><?php echo $userObj[0]->first_name; ?>
							&nbsp;&nbsp;<?php echo $userObj[0]->last_name; ?></span></button>
							<button type="button" style="height:44px" class="btn btn-danger btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="<?php echo base_url() ?>homeController">กลับหน้าหลัก</a></li>
								<li><a href="<?php echo base_url() ?>AbsenceController/create">เขียนใบลา</a></li>
								<li><a href="<?php echo base_url() ?>AbsenceController">ประวัติการลา</a></li>
								<?php if ($userObj[0]->role == 'boss'): ?>
									<li><a href="<?php echo base_url();?>AbsenceController">อนุมัติการลา <span style="color:red;margin-left:20px"><?php if($countntf != "0"){echo $countntf;}  ?></span></a></li>
								<?php endif; ?>
								<?php if ($userObj[0]->role == 'admin'): ?>
									<li><a href="<?php echo base_url();?>MenuUserController/index">รายชื่อผู้ใช้งาน</a></li>
									<li><a href="<?php echo base_url() ?>MenuUserController/showadd">เพิ่มผู้ใช้งาน</a></li>
								<?php endif; ?>
								<li role="separator" class="divider"></li>
								<li><a href="<?php echo base_url().'EmployeeController?id='.$userObj[0]->id ?>">แก้ไขข้อมูลส่วนตัว</a></li>
								<li><a href="<?php echo base_url();?>homecontroller/logout">ออกจากระบบ</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<a class="btn btn-info" href="<?php echo base_url() . 'homeController' ?>" role="button"><span class="glyphicon glyphicon-home"></span> กลับหน้าหลัก</a>

							</div>
							<?php if ($userObj[0]->role != 'boss' && $userObj[0]->role != 'admin'): ?>
								<!-- Button trigger modal -->
								<div class="col-md-6" align="right">
									<a  class="btn btn-info " align="right" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-comment"></span> ประวัติการลาทั้งหมดของคุณ</a>
								</div>
							</div>
							<!-- Modal -->
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">ประวัติการลาทั้งหมดของคุณ</h4>
										</div>
										<div class="modal-body">
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-5">

														<h4 align="right">ลาป่วย</h4><br>
														<h4 align="right">ลากิจ</h4><br>
														<h4 align="right">ลาพักร้อน</h4><br>
														<h4 align="right" style="color:red">จำนวนการลาทั้งหมด</h4>
													</div>
													<div class="col-md-1">

													</div>
													<div class="col-md-2">

													</div>
													<div class="col-md-4">
														<h4><?php echo $rest1; ?> ครั้ง</h4><br>
														<h4><?php echo $rest2; ?> ครั้ง</h4><br>
														<h4><?php echo $rest3; ?> ครั้ง</h4><br>
														<h4 style="color:red"><?php echo $total_row; ?> ครั้ง</h4>
													</div>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							<?php endif; ?>
						</div>

						<div class="panel panel-default" style="margin-top:1%">
							<div class="panel-heading" id="div_"><?php if ($userObj[0]->role != 'boss'): ?>
								คุณมีใบลาในระบบทั้งหมด
								<?php echo $total_row; ?>  ใบ
								<!-- <span id="sp_right">การลา <?php echo $total_row; ?> ครั้ง ลาป่วย <?php echo $rest1; ?> ครั้ง ลากกิจ <?php echo $rest2?> ครั้ง ลาพักร้อน <?php echo $rest3?> ครั้ง</span></div> -->
							<?php endif; ?>
							<div class="panel-body">

								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>สถาณะ</th>
											<th>ประเภทการลา</th>
											<th>วันที่เริ่มต้นการลา</th>
											<th>วันที่สิ้นสุดการลา</th>
											<th>จำนวนวันที่ลา</th>
											<th>เหตุผลในการลา</th>
											<th>
												ส่วนการทำงาน
											</th>
											<th>

											</th>
										</tr>
									</thead>
									<tbody>
										<?php for ($i = 0; $i < count($absencelist); ++$i) { ?>
											<tr>
												<td class="col-md-1"><?php echo ($page+$i+1); ?></td>
												<td class="col-md-1">
													<?php if ($absencelist[$i]->status == 'wait'): ?>
														<span class="label label-danger">รอการอนุมัติ</span>
													<?php elseif($absencelist[$i]->status === 'disapprove'): ?>
															<span class="label label-danger">ไม่อนุมัติ</span>
													<?php else: ?>
														<span class="label label-success">อนุมัติ</span>
													<?php endif ?></td>
													<td class="col-md-1"><?php echo $absencelist[$i]->label; ?></td>
													<td class="col-md-1"><?php echo formatDateToShow($absencelist[$i]->start); ?></td>
													<td class="col-md-1"><?php echo formatDateToShow($absencelist[$i]->end); ?></td>
													<td class="col-md-1"><?php echo round($absencelist[$i]->total); ?> วัน</td>
													<td class="col-md-4">
														<div class="well"><?php echo $absencelist[$i]->reason; ?></div>
													</td>
													<td class="col-md-3">
														<?php $userObj  = $this->session->userdata('userLogin'); ?>
														<?php if ($userObj[0]->id != $absencelist[$i]->employee_id ): ?>
															<?php if ($userObj[0]->role == 'boss'): ?>
																<a href="<?php echo base_url() . 'absenceController/approved/' . $absencelist[$i]->id . '/' . $absencelist[$i]->status ?>" style="color:green" >
																	<?php if ($absencelist[$i]->status == 'approved'): ?>
																		<span class="glyphicon glyphicon-import">ยกเลิการอนุมิตื</span>
																	<?php elseif($absencelist[$i]->status == 'disapproved'): ?>

																	<?php else: ?>
																		<span class="glyphicon glyphicon-saved">อนุมัติ</span>
																	<?php endif; ?>
																</a>
															<?php endif; ?>
															<?php if ($userObj[0]->role == 'user' and  $absencelist[$i]->status!= 'approved' ): ?>
																<a href="<?php echo base_url() . 'absenceController/delete/' . $absencelist[$i]->id ?>"  style="color:red">
																	<span class="glyphicon glyphicon-remove"></span>
																</a>
															<?php endif; ?>
															<?php if ($userObj[0]->role == 'boss'): ?>
																<a href="<?php echo base_url() . 'absenceController/disapproved/' . $absencelist[$i]->id ?>"  style="color:red">
																	<span class="glyphicon glyphicon-remove">ไม่อนุมัติ</span>
																</a>
															<?php endif; ?>
														<?php endif; ?>
													</td>
													<td class="col-md-2">
														<a href="<?php echo base_url()?>rpt/testrpt.php?id=<?php echo $absencelist[$i]->id ?>"><img align="right" width="30px" height="30px" style="margin-right:2%" src="<?php echo base_url()?>img/pdf.png" ></a>
													</td>
													<?php if ($absencelist[$i]->att_file != 'noattfile'): ?>
														<td>
															<a href="<?php echo base_url() . $absencelist[$i]->att_file ; ?>" style="color:red" >
																<span class="glyphicon glyphicon-cloud-download"></span>
															</a>
														</td>
													<?php endif; ?>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
									<div class="row">
										<div class="col-md-12 text-center">
											<?php echo $pagination; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</body>
				</html>
