<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม </title>

  <?php include_once 'scriptandcss.php'; ?>



  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
  <?php include_once 'submenu.php'; ?>
  <div class="container">
    <div class="row">
      <div class="panel pandel-default">
        <div class="panel-body">
          <div class="col-md-12">
            <button type="button" cclass="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">เพิ่มวันหยุด</button></br></br></br></br>
            <table id="dt_dayoff" class="table" >
              <thead>
                <tr>
                  <th>#</th>
                  <th>วันที่</th>
                  <th>ป้ายกำกับ</th>
                  <th>
                  </th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">เพิ่มวันหยุด</h4>
          </div>
          <div class="modal-body">
            <form class="form-inline" id="frm-dayoff">
              <div class="form-group">
                <label class="sr-only" >ป้ายกำกับวันหยุด</label>
                <input type="text" class="form-control"  name="label" placeholder="ป้ายกำกับ" >
              </div>
              <div class="form-group">
                <label class="sr-only">วันที่</label>
                <input type="text" class="form-control" id="pickdate" name="date" placeholder="วันที่">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
            <button type="button" class="btn btn-primary" id="btn-add">บันทึก</button>
          </div>
        </div>
      </div>
    </div>



  </div>
</div>
</body>
</html>

<!--script src="../js/bootstrap-datepicker-thai.js" charset="utf-8"></script-->

<script type="text/javascript">
$(document).ready(function(){
  $('#pickdate')
  .datepicker({
    language:'th',
    format:'dd-mm-yyyy',
    orientation:'bottom',
    autoclose:true,
  })
  .on('changeDate',function(){
    $.ajax({
      url: 'DayOffCtrl/isDup?date='+$('#pickdate').val()
    })
    .done(function(data){
      if(data.response){
        alert(data.data+" ถูกเพิ่มไปแล้ว กรุณาเลือกใหม่");
        $('#pickdate').val('');
        $('#pickdate').focus();
      }
    })
  });
  var dt = $('#dt_dayoff').DataTable({
    ajax : "DayOffCtrl/getAll",
    pageLength : 10,
    columns:[
      {data: null},
      {data: 'date'},
      {data: 'label'},
      {
        mRender : function(data,type,full){
          return '<a class="btn btn-default" data-id="'+full.id+'"  data-action="delete" href="#" role="button">ลบ</a>';
        }
      }

    ],
    language: {
      "lengthMenu": "แสดง _MENU_ รายการ ต่อ หน้า",
      "zeroRecords": "ไม่พบ - ขอภัย",
      "info": "กำลังแสดงหน้า_PAGE_ จาก _PAGES_",
      "infoEmpty": "ไม่พบผลลัพธ์....",
      "infoFiltered": "(กรอง จาก _MAX_ รายการ)",
      "search":         "ค้นหา:",
      "paginate": {
        "first":      "หน้าแรก",
        "last":       "หน้าสุดท้าย",
        "next":       "ถัดไป",
        "previous":   "ก่อนหน้า"
      }
    }
  });

  dt.on( 'order.dt search.dt', function () {
    dt.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
      cell.innerHTML = i+1;
    } );
  } ).draw();

  $('#dt_dayoff').on('click','a',function(){
    var action =  $(this).data('action');
    var id = $(this).data('id');
    if(action=='edit'){
      console.log('update?'+id);
    }
    else if(action == 'delete'){
      console.log('delete?'+id);
      $.ajax({
        url:'DayOffCtrl/delete?id='+id
      }).done(function(){
        alert('ทำการลบข้อมูลสำเร็จ');
        dt.ajax.reload();
      });
    }
  });

  $('#btn-add').click(function(){

    if($('input[name="label"]').val() != '' && $('input[name="date"]').val() != ''){
      $.ajax({
        method:'POST',
        url : 'DayOffCtrl/add',
        data : $('#frm-dayoff').serialize()
      })
      .done(function(){
        alert('ทำการบันทึกข้อมูลเรียบร้อย');
        dt.ajax.reload();
        $('#frm-dayoff').trigger('reset');
      });
    }else{
      alert('กรุณากรอกข้อมูลให้ครบถ้วนก่อนทำรายการ');
    }

  });

  $('input[type="search"]').datepicker({
    language:'th',
    format:'dd-mm-yyyy',
    orientation:'bottom',
    autoclose:true,
  }).on('changeDate',function(){
    dt.search($('input[type="search"]').val()).draw();
  });

});


</script>
