<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>ระบบใบลาออนไลน์  มหาวิทยาลัยสยาม</title>

	<!-- Bootstrap Core CSS -->
	<link href="<?php echo base_url() ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Font Awesome CSS -->
	<link href="<?php echo base_url() ?>/css/font-awesome.min.css" rel="stylesheet">

	<style>
		@media print {
			* {
				color: #000 !important;
				text-shadow: none !important;
				background: transparent !important;
				box-shadow: none !important;
			}
		}
	</style>

	<style>
		body
		{
			padding-top: 4%;
			padding-right: 15%;                 
			padding-left: 17%;
		}
		input[type=checkbox]
		{

			/* Double-sized Checkboxes */
			-ms-transform: scale(2); /* IE */
			-moz-transform: scale(2); /* FF */
			-webkit-transform: scale(2); /* Safari and Chrome */
			-o-transform: scale(2); /* Opera */
			padding: 10px;
		}
		p.subtext
		{
			padding-left: 2%;
			font-weight: bold;
		}
		.rec{

			position: relative;
			z-index: -1;
		}
		body {
			margin: 0px 0px 0px 0px;
		}

	</style>


</head>
<body>

	<!-- <div class="container" style="margin-right:10%"> -->
	<!-- Header ใบลา -->
	<div id="headerLT" class="row" style="padding:10" align="center" >
		<h4 align="right" style="padding-right:45%">ใบลาพักผ่อนประจำปี</h4>
	</br>
	<p align="right" style="margin-right:15%">เขียนที่................................</p>
</br>
<p align="right" style="margin-right:15%">วันที่........เดือน............................พ.ศ...................</p>
</div>
<!-- END Header ใบลา -->

<br>

<!-- SubHeader หัวข้อ -->
<div id="subheadLT" class="container" style="">
	<p align="left" class="subtext">เรื่อง  ขอใบลาพักผ่อนประจำปี</p>0
	<p align="left" class="subtext">เรียน ....................................................</p>
</div>
<!-- END SubHeader หัวข้อ --> 

<br>
<!-- Content -->
<div id="contenLT" class="row">
	<p align="left" class="" style="margin-left:10%; margin-right:10%">
		ด้วยข้าพเจ้า นาย / นาง / นางสาว.............................................................................................................................ตำแหน่ง
	</p>
	<p align="left" class="" style="margin-left:2%; margin-right:10%">
		........................................................................สังกัด................................................................มีวันลาพักผ่อนสะสม.................วันทำการ
	</p>
	<p align="left" class="" style="margin-left:2%; margin-right:10%">
		มีสิทธิลาพักผ่อนประจำปีนี้อีก................วันทำการ.วันทำการ       รวมเป็น...............วันทำการวันทำการ     ขอลาพัก    ตั้งแต่วันที่..................................
	</p>
	<p align="left" class="" style="margin-left:2%; margin-right:10%">
		เดือน...............................พ.ศ.................ถึงวันที่..............เดือน..............................พ.ศ...............มีกำหนด. 
	</p>
	<p align="left" class="" style="margin-left:2%; margin-right:10%">
		..........วัน  ในระหว่างลาจะติดต่อข้าพเจ้าได้ที่.............................................................................................................................................
	</p>
	<p align="left" class="" style="margin-left:2%; margin-right:10%">
		........................................................................................................................................................................................................
	</p>
	<p align="left" class="" style="margin-left:2%; margin-right:10%">
		........................................................................................................................................................................................................
	</p>            
</div>
<!-- END Content -->
<br>
<br>

<!-- ลงชื่อ -->
<div class="row">
	<p class="subtext" align="right" style="margin-right:35%">
		ขอแสดงความนับถือ
	</p>
	<p class="subtext" align="right" style="margin-right:30%">
		(ลงชื่อ)..................................................
	</p>
	<p class="subtext" align="right" style="margin-right:30%">
		(.........................................................)
	</p>          
</div>
<!-- END ลงชื่อ -->
<br>
<br>

<!-- footerLT -->
<div class="row">
	<p id="">
		<span style="text-decoration: underline;margin-left:2%;font-weight: bold;">
			สถิติการลาในการลาในปีการศึกษานี้
		</span>
		<span style="margin-left:45%; font-weight: bold">
			ความเห็นผู้บังคับบัญชา
		</span>
	</p>
	<br>
	<table border="1px" width="40%" style="margin-left:2%">
		<tr align="center" style="font-weight: bold;">
			<td height="30">ลามาแล้ว</td>
			<td height="30">ลาครั้งนี้</td>
			<td height="30">รวมเป็น</td>
		</tr>
		<tr align="center">
			<td height="30">(วันทำการ)</td>
			<td height="30">(วันทำการ)</td>
			<td height="30">(วันทำการ)</td>                
		</tr>
		<tr>
			<td height="30"></td>
			<td height="30"></td>
			<td height="30"></td>
		</tr>
	</table>
	<br>
	<p class="subtext" align="right" style="margin-right:14%">...........................................................</p>
	<p class="subtext" align="right" style="margin-right:14%">......................................................................</p>
</div>
<!-- END footerLT -->
<br>
<!-- ลงชื่อ -->
<div class="row">
	<div class="col-lg-6" align="left" style="">
		<p class="subtext">(ลงชื่อ)..........................................ผู้ตรวจสอบ</p>
		<p class="subtext">ตำแหน่ง.................................................</p>
		<p class="subtext">วันที่.............../...................../..................</p>
	</div>
	<div class="col-lg-6" align="right" style="padding-right:14%">
		<p class="subtext">(ลงชื่อ)................................................</p>
		<p class="subtext">ตำแหน่ง....................................................</p>
		<p class="subtext">วันที่............./.................../...............</p>
		<br>
		<p class="subtext" align="left" style="text-decoration: underline">คำสั่ง</p>
		<label style="padding-right:20%">
			<input type="checkbox"/>&nbsp;&nbsp;อนุญาต 
		</label>
		<label style="padding-right:14%">
			<input type="checkbox"/>&nbsp;&nbsp;ไม่อนุญาต 
		</label>          
	</div>
</div>
<!-- ลงชื่อ -->
<div class="row">
	<div class="col-lg-6" align="left" style="">

	</div>
	<div class="col-lg-6" align="right" style="padding-right:14%">

		<p class="subtext">...........................................................</p>
		<p class="subtext">......................................................................</p>
		<br>
		<p class="subtext">(ลงชื่อ)................................................</p>
		<p class="subtext">ตำแหน่ง....................................................</p>
		<p class="subtext">วันที่............./.................../...............</p>
	</div>
</div>
<!-- </div> -->


</body>
</html>