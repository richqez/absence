
<!-- Favicon and touch icons -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-57-precomposed.png">



<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url() ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome CSS -->
<link href="<?php echo base_url() ?>/css/font-awesome.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo base_url() ?>/css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo base_url() ?>/css/style.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>

<!-- Custom CSS -->
<link href="<?php echo base_url() ?>/css/datatables.min.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url() ?>/css/bootstrap-datepicker.css" media="screen" title="no title" charset="utf-8">

<style media="screen">
.navbar {
  border-radius: 0px;
}
</style>

<!-- Template js -->

<script src="<?php echo base_url() ?>/js/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url() ?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>/js/jquery.appear.js"></script>
<script src="<?php echo base_url() ?>/js/jqBootstrapValidation.js"></script>
<script src="<?php echo base_url() ?>/js/modernizr.custom.js"></script>
<script src="<?php echo base_url() ?>/js/script.js"></script>
<script src="<?php echo base_url() ?>/js/datatables.min.js" charset="utf-8"></script>
<script src="<?php echo base_url() ?>/js/bootstrap-datepicker.js" charset="utf-8"></script>
<script src="<?php echo base_url() ?>/js/bootstrap-datepicker.th.js" charset="utf-8"></script>
<script src="<?php echo base_url() ?>/js/jquery.autocomplete.min.js"></script>
