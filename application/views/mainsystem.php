
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ระบบใบลาออนไลน์ - มหาวิทยาลัยสยาม</title>

  <?php include_once 'scriptandcss.php'; ?>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
  <?php $userObj  = $this->session->userdata('userLogin'); ?>
  <?php include_once 'submenu.php'; ?>
  <!-- Start Logo Section -->

  <!-- Start Main Body Section -->
  <div class="mainbody-section text-center">
    <div class="container">
      <div class="row">

          <div class="col-md-6" style="margin-top:8%;margin-left:22%">
            <!-- Start Carousel Section -->

            <!-- Start Carousel Section -->

            <div class="row">
              <div class="col-md-6">

                  <div class="menu-item  light-red">
                    <a href="<?php echo base_url() ?>DayOffCtrl">
                      <i class="fa fa-check"></i>
                      <p>จัดการวันหยุด</p>
                    </a>
                  </div>

              </div>
              <div class="col-md-6">
                <?php if ($userObj[0]->role == 'admin'): ?>
                  <div class="menu-item color">
                    <a href="<?php echo base_url();?>MenuUserController/showadd" >
                      <i class="fa fa-plus-circle"></i>
                      <p>เพิ่มผู้ใช้งาน</p>
                    </a>
                  </div>
                <?php endif; ?>
              </div>
              <div class="col-md-6">
                <!--  menu for boss-->
                <!-- show for boss only -->

                  <div class="menu-item light-orange responsive-2">
                      <a href="<?php echo base_url() ?>MenuUserController/index">
                        <i class="fa fa-book">

                        </i>
                      <p>รายชื่อผู้ใช้</p>
                    </a>
                  </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </body>

  </html>
