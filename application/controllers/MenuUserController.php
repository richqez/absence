<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MenuUserController extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->helper('date');

	}

	public function index(){

		//$this->load->model('Employee_Model');
		$data['countntf'] = $this->absence_model->Notifications();
		$data['query'] = $this->employee_model->showuser();
		$this->load->view('absence_listuser',$data);

	}
	public function showadd(){
		$this->load->model('Agencies_Model');
		$data['faculty'] = $this->agencies_model->facultyAll();
		$data['branch'] = $this->agencies_model->branchAll();
		$data['countntf'] = $this->absence_model->Notifications();
		$this->load->view('absence_user',$data);

	}


	public function adduser(){


		$this->employee_model->add();
		redirect('MenuUserController/index');
	}
	public function del($id){

	 	//$this->load->model('Employee_Model');
    	$this->employee_model->delete($id);
		redirect('MenuUserController/index');
	}
	public function edit($id)
	{
		//$this->load->model('Employee_Model');
		$data['countntf'] = $this->absence_model->Notifications();
		$data['query'] = $this->employee_model->edituser($id);
		$this->load->view('absence_user',$data);
	}

	public function getBranch(){
		$facultyId = $this->input->post('faculty_id');
		$query = $this->db->get_where('branch',array("ref_faculty" => $facultyId));
		header('Content-type: application/json;charset=utf-8');
		echo json_encode(array('data'=>$query->result()));
	}

}

/* End of file MenuUserController.php */
/* Location: ./application/controllers/MenuUserController.php */
