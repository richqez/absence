<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DayOffCtrl extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {
    $this->load->view('dayoff');
  }

  public function getAll(){

    $data = $this->db->get('t_dayoff')->result();


    foreach ($data as $key => $value) {
      $value->date = formatDateToShow($value->date);
    }

    header('Content-type: application/json;charset=utf-8');
    echo json_encode(array("data"=>$data));
  }

  public function isDup(){
    $isDup =true ;
    $date = formatDateToSave($this->input->get('date'));
    $count = $this->db->get_where('t_dayoff',array('date'=>$date))->num_rows();

    if ($count < 1) {
      $isDup = false ;
    }

    header('Content-type: application/json;charset=utf-8');
    echo json_encode(array('response' => $isDup ,'data' => formatDateToShow($date) ,'count' => $count));

  }


  public function add(){

    $data = array('id' => '', 'label'=> $this->input->post('label') , 'date' => formatDateToSave($this->input->post('date')) );
    $this->db->insert('t_dayoff',$data);

  }

  public function update(){
    $data = array('label'=> $this->input->post('label') , 'date' => formatDateToSave($this->input->post('date')) );
    $this->db->update('t_dayoff',$data, array('id' => $this->input->post('id')));

  }

  public function delete(){

    $this->db->delete('t_dayoff',array('id'=>$this->input->get('id')));

  }




}
