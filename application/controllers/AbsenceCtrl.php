<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AbsenceCtrl extends CI_Controller{


  public $withuser = 'no';


  public function __construct()
  {
    parent::__construct();
    $this->load->model('agencies_model');

    //Codeigniter : Write Less Do More


  }

  function index()
  {

    $this->session->set_userdata('withuser',$this->input->get('withuser'));
    $this->session->set_userdata('isforshowall',$this->input->get('withuser'));


    $data['rest1'] = $this->absence_model->countRest1();
    $data['rest2'] = $this->absence_model->countRest2();
    $data['rest3'] = $this->absence_model->countRest3();
    $data['type'] = $this->db->get('type_of_leave')->result();
    $data['countntf'] = $this->absence_model->Notifications();
    $data['branch'] = $this->agencies_model->branchAll();
    $this->load->view('absence_list2',$data);

  }

  public function dataTable(){

    $this->db->select('absence.id as a_id , absence.* , type_of_leave.label,branch.name_b,employee.first_name');
    $this->db->join('type_of_leave', 'type_of_leave.id = absence.type_of_leave_id');
    $this->db->join('employee','employee.id = absence.employee_id');
    $this->db->join('branch','branch.id_b = employee.branch_id');

    if ($this->session->userdata('withuser')=='yes') {
      $userObj  = $this->session->userdata('userLogin');
      $this->db->where(array('employee_id'=>$userObj[0]->id));
    }

    $this->db->from('absence');
    $result = $this->db->get()->result();
    foreach ( $result as $key => $value) {
      $value->start = formatDateToShow($value->start);
      $value->end  = formatDateToShow($value->end);
    }
    header('Content-type: application/json;charset=utf-8');
    echo json_encode(array("data"=>  $result));
  }


}
?>
