<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AbsenceController extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->helper('date');


	}

	public function index(){

		$userObj  = $this->session->userdata('userLogin');
		//pagination settings
		$config['base_url'] = base_url() . 'absenceController/index';

		$config['per_page'] = "20";
		$config["uri_segment"] = 3;

		//config for bootstrap pagination class integration
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = false;
		$config['last_link'] = false;
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';



		$data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$state = ($this->uri->segment(7)) ? $this->uri->segment(7) :0;

		if ($userObj[0]->role=='user' OR $state == '9') {

			$data['absencelist'] = $this->absence_model->
			findForList(
			$data['page'],
			$config["per_page"],
			($this->uri->segment(5)) ? $this->uri->segment(5) : null);

			$x= $this->uri->segment(5) ? $this->uri->segment(5) : null;
			//	var_dump($x);
			//var_dump($state);


			$config['total_rows'] = $this->absence_model->countByUser();

		}
		else if($userObj[0]->role!='user' ) {
			$data['absencelist'] = $this->absence_model->findForAppoved($data['page'],$config["per_page"]);
			$config['total_rows'] = $this->absence_model->countAll();
		}





		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = floor($choice);
		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];
		$data['rest1'] = $this->absence_model->countRest1();
		$data['rest2'] = $this->absence_model->countRest2();
		$data['rest3'] = $this->absence_model->countRest3();
		$data['countntf'] = $this->absence_model->Notifications();
		$data['selectType'] = $this->absence_model->findAll();
		$this->load->view('absence_list', $data);


		//var_dump($config['total_rows']);
	}


	public function create(){
		$data['countntf'] = $this->absence_model->Notifications();
		$data['selectType'] = $this->absence_model->findAll();
		$this->load->view('absence_form',$data);
	}

	public function getReport(){
		$this->load->view('absence_report');
	}


	public function delete($id){
		$config["uri_segment"] = 5;
		$this->absence_model->delete($id);
		redirect('absenceController');
	}

	public function approved(){

		$data = array('status' => "approved"
		,"approve_reason" => $this->input->post('reason')
		,"approved_by" => $this->input->post('appby')
		,"approve_date"=>date("Y-m-d"));

		$this->db->where('id',$this->input->post('id'));
		$this->db->update('absence',$data);


	}

	public function unappoved(){
		$this->absence_model->unappoved($this->input->get('id'));
		if ($this->session->userdata('withuser')=='no') {
			redirect('AbsenceCtrl?withuser=no');
		}
		else{
			redirect('AbsenceCtrl?withuser=yes');
		}
	}



	public function disapproved(){
		$data = array('status' => "disapproved"
		,"approve_reason" => $this->input->post('reason')
		,"approved_by" => $this->input->post('appby')
		,"approve_date"=>date("Y-m-d"));

		$this->db->where('id',$this->input->post('id'));
		$this->db->update('absence',$data);

	}




	public function checkDate(){

		$res = false ;

		$x = formatDateToSave($this->input->post('start')) ;
		$y = formatDateToSave($this->input->post('end')) ;
		$t = $this->input->post('type');
		$z = getDateRange($x,$y);

		$userObj  = $this->session->userdata('userLogin');
		$this->db->select_sum('absence.total');
		$this->db->where(array('employee_id'=>$userObj[0]->id,'type_of_leave_id'=>$t ));
		$d = $this->db->get('absence')->result();
	//	var_dump($d);
		$sumdate = $z + $d[0]->total;

		switch ($t) {
			case 1:
				if ($sumdate <= 6) {
					$res = true;
				}
				break;
			case 2:
			if ($sumdate <= 6) {
				$res = true;
			}
				break;
			case 3:
			if ($sumdate <= 30) {
				$res = true;
			}
				break;
			default:
				# code...
				break;
		}


				header('Content-type: application/json;charset=utf-8');
				echo json_encode(array('status' => $res , 'xdata' => $sumdate ));
	}




	public function save(){


			$this->absence_model->insert();
			redirect('AbsenceCtrl?withuser=yes');




	}

	public function result(){
		$this->load->view('absence_result');
	}

	public function mainsys()
	{
		$this->load->view('mainsystem');
	}




}

/* End of file absenceController.php */
/* Location: ./application/controllers/absenceController.php */
