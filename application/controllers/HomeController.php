<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HomeController extends CI_Controller {


	public function index()
	{
		$data['countntf'] = $this->absence_model->Notifications();
		$this->checkLoginStatus();

	}

	public function login(){

		if ($this->session->userdata('userLogin')!=null) {
			$data['countntf'] = $this->absence_model->Notifications();
			$this->load->view('dash',$data);
		}else{

			$result = $this->employee_model->findByUser();

			if ($result!=null) {
				$this->saveToSesstion($result);
				$this->index();
			}
			else{
				$data['isError'] = true;
				$this->load->view('login', $data);
			}
		}

	}

	public function logout(){
		$this->session->unset_userdata('userLogin');
		$this->index();
	}

	private function saveToSesstion($employee){

		$this->session->set_userdata('userLogin',$employee);

	}

	public function checkLoginStatus(){
		if ($this->session->userdata('userLogin')!=null) {
				$data['countntf'] = $this->absence_model->Notifications();
			$this->load->view('dash',$data);
		}
		else{
			$this->load->view('login');
		}
	}

	public function getRole(){
		echo $this->session->userdata('withuser');
	}

}
