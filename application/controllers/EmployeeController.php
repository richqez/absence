<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EmployeeController extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model('agencies_model');
    //$this->load->model('employee_model');
  }

  function index()
  {
    $data['employee'] = $this->employee_model->findById($this->input->get('id'))[0];
    $data['agencies'] = $this->agencies_model->findAll();
    $data['faculty'] = $this->agencies_model->facultyAll();
    $data['branch'] = $this->agencies_model->branchAll();
    $data['countntf'] = $this->absence_model->Notifications();
    $this->load->view('employee_update',$data);
  }

  function update(){
    $this->employee_model->update();
    $id = $this->input->post("id");

    $this->session->set_userdata('userLogin',$this->db->get_where("employee",array("id"=>$id))->result());
    echo '<script>alert("ยินดีด้วย แก้ไขข้อมูลส่วนตัวสำเร็จแล้ว");</script>';
    redirect('HomeController/checkLoginStatus');
  //$this->home();

  }
  function  home()
  {
    if ($this->session->userdata('userLogin')!=null) {
      $data['countntf'] = $this->absence_model->Notifications();
			$this->load->view('dash');
		}else{

			$result = $this->employee_model->findByUser();

			if ($result!=null) {
				$this->saveToSesstion($result);
				$this->index();
			}
			else{
				$data['isError'] = true;
				$this->load->view('login', $data);
			}
		}
  }

}
