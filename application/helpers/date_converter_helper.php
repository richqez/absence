<?php


function chekOffDay($date){

	 $CI =& get_instance();
	$data = $count = $CI->db->get_where('t_dayoff',array('date'=>$date))->num_rows();
	if ($data < 1) {
		return false;
	}else {
		return true ;
	}
}

function getDateRange($s,$e)
{
	$strStartDate = $s;
	$strEndDate = $e;

	$intWorkDay = 0;
	$intHoliday = 0;
	$intPublicHoliday = 0 ;
	$intTotalDay = ((strtotime($strEndDate) - strtotime($strStartDate))/  ( 60 * 60 * 24 )) + 1;

	while (strtotime($strStartDate) <= strtotime($strEndDate)) {

		$DayOfWeek = date("w", strtotime($strStartDate));
		if($DayOfWeek == 0 or $DayOfWeek ==6)
		{
			$intHoliday++;
		}
		else if(chekOffDay($strStartDate)){
			$intPublicHoliday++;
		}
		else
		{
			$intWorkDay++;
		}

		$strStartDate = date ("Y-m-d", strtotime("+1 day", strtotime($strStartDate)));
	}

	return ($intTotalDay-$intHoliday)-$intPublicHoliday;
}

function formatDateToSave($source){

	$splitSource = explode("-", $source);
  $dd = $splitSource[0];
  $mm = $splitSource[1];
  $yyyy = $splitSource[2] - 543;
  return  $yyyy. '-' . $mm . '-' . $dd ;

}



function formatDateToShow($source){

	$splitSource = explode("-", $source);
  $dd = $splitSource[2];
  $mm = $splitSource[1];
  $yyyy = $splitSource[0] + 543;
  return  $dd. '-' . $mm . '-' . $yyyy ;

}

?>