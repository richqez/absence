-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 28, 2015 at 07:44 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `absence`
--

-- --------------------------------------------------------

--
-- Table structure for table `absence`
--

CREATE TABLE `absence` (
  `id` int(11) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `total` int(11) DEFAULT NULL,
  `reason` varchar(500) DEFAULT NULL,
  `employee_id` int(11) NOT NULL,
  `type_of_leave_id` int(11) NOT NULL,
  `status` varchar(10) DEFAULT 'wait',
  `approved_by` int(11) DEFAULT NULL,
  `docdate` date NOT NULL,
  `att_file` text,
  `approve_reason` varchar(250) NOT NULL,
  `approve_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `absence`
--

INSERT INTO `absence` (`id`, `start`, `end`, `total`, `reason`, `employee_id`, `type_of_leave_id`, `status`, `approved_by`, `docdate`, `att_file`, `approve_reason`, `approve_date`) VALUES
(23, '0000-00-00', '0000-00-00', 1, 'asdf', 10, 1, 'approved', 11, '0000-00-00', NULL, '', '2015-09-14'),
(31, '2015-08-31', '2015-09-01', 2, 'หหหหหห', 9, 1, 'approved', 11, '0000-00-00', 'att_file/1441037055.rar', 'asdsfa', '0000-00-00'),
(32, '2015-08-31', '2015-09-01', 2, 'glyphicon glyphicon-cloud-downloadglyphicon glyphicon-cloud-downloadglyphicon glyphicon-cloud-downloadglyphicon glyphicon-cloud-downloadglyphicon glyphicon-cloud-downloadglyphicon glyphicon-cloud-downloadglyphicon glyphicon-cloud-downloadglyphicon glyphicon-cloud-downloadglyphicon glyphicon-cloud-downloadglyphicon glyphicon-cloud-downloadglyphicon glyphicon-cloud-downloadglyphicon glyphicon-cloud-download', 9, 1, 'approved', 11, '0000-00-00', 'noattfile', 'asdffff', '0000-00-00'),
(33, '2015-09-15', '2015-09-16', 2, 'tqaewtgetgetetet', 9, 1, 'approved', 11, '0000-00-00', 'noattfile', 'fgeagegegegeg', '0000-00-00'),
(34, '2015-10-28', '2015-10-29', 1, 'DayOffCtrl', 10, 2, 'wait', NULL, '0000-00-00', 'att_file/1446013098.png', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id_b` int(5) NOT NULL,
  `name_b` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ref_faculty` varchar(5) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id_b`, `name_b`, `ref_faculty`) VALUES
(3, 'วิทยาการคอมพิวเตอร์', '1'),
(4, 'เทคโนโลยีการอาหาร', '1'),
(5, 'IT', '2');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `sex` char(1) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `type_of_agencies_id` int(11) DEFAULT NULL,
  `position` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  `img_path` text NOT NULL,
  `emp_id` text NOT NULL,
  `branch_id` varchar(5) DEFAULT NULL,
  `faculty_id` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `user_name`, `password`, `first_name`, `last_name`, `sex`, `address`, `email`, `type_of_agencies_id`, `position`, `role`, `img_path`, `emp_id`, `branch_id`, `faculty_id`) VALUES
(9, 'user', '1234', 'user', 'user', 'm', '1234', 'test@email.com', 1, 'นักศึกษา', 'user', '', '1234', '4', '1'),
(10, 'admin', '1234', 'admin', 'admin', 'm', '215125', '1234@1234.1234', 1, 'ผู้ดูแลระบบ', 'admin', '', '1234', '3', '1'),
(11, 'test', '1234', 'test', 'test', 'm', 'test', 'test@test@test', 1, 'boss', 'boss', '', '54', '3', '1'),
(12, 'DayOffCtrl', 'DayOffCtrl', 'DayOffCtrl', 'DayOffCtrl', 'm', 'DayOffCtrl', 'DayOffCtrl', 1, 'DayOffCtrl', 'admin', '', '550480006', '3', '1 ');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id_f` varchar(5) CHARACTER SET utf8 NOT NULL,
  `name_f` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id_f`, `name_f`) VALUES
('1', 'วิทยาศาสตร์'),
('2', 'เทคโนโลยีสารสนเทศ');

-- --------------------------------------------------------

--
-- Table structure for table `type_of_agencies`
--

CREATE TABLE `type_of_agencies` (
  `id_agen` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type_of_agencies`
--

INSERT INTO `type_of_agencies` (`id_agen`, `name`, `description`) VALUES
(1, 'วิทยาศาสาตร์', 'test'),
(2, 'เทคโนโลยีสารสนเทศ', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `type_of_leave`
--

CREATE TABLE `type_of_leave` (
  `id` int(11) NOT NULL,
  `label` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type_of_leave`
--

INSERT INTO `type_of_leave` (`id`, `label`, `description`) VALUES
(1, 'ลาป่วย', 'test'),
(2, 'ลากิจ', 'test'),
(3, 'ลาพักร้อน', 'ลาพักร้อน');

-- --------------------------------------------------------

--
-- Table structure for table `t_dayoff`
--

CREATE TABLE `t_dayoff` (
  `id` int(11) NOT NULL,
  `label` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_dayoff`
--

INSERT INTO `t_dayoff` (`id`, `label`, `date`) VALUES
(2, 'dsad', '2015-10-28'),
(3, 'ทดสอบ', '2015-10-24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absence`
--
ALTER TABLE `absence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_absence_employee_idx` (`employee_id`),
  ADD KEY `fk_absence_type_of_leave1_idx` (`type_of_leave_id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id_b`),
  ADD KEY `id` (`id_b`),
  ADD KEY `ref_faculty` (`ref_faculty`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_employee_type_of_agencies1_idx` (`type_of_agencies_id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `faculty_id` (`faculty_id`),
  ADD KEY `branch_id_2` (`branch_id`),
  ADD KEY `faculty_id_2` (`faculty_id`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id_f`),
  ADD KEY `id` (`id_f`);

--
-- Indexes for table `type_of_agencies`
--
ALTER TABLE `type_of_agencies`
  ADD PRIMARY KEY (`id_agen`);

--
-- Indexes for table `type_of_leave`
--
ALTER TABLE `type_of_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_dayoff`
--
ALTER TABLE `t_dayoff`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absence`
--
ALTER TABLE `absence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id_b` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `type_of_agencies`
--
ALTER TABLE `type_of_agencies`
  MODIFY `id_agen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `type_of_leave`
--
ALTER TABLE `type_of_leave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_dayoff`
--
ALTER TABLE `t_dayoff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `absence`
--
ALTER TABLE `absence`
  ADD CONSTRAINT `fk_absence_employee` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_absence_type_of_leave1` FOREIGN KEY (`type_of_leave_id`) REFERENCES `type_of_leave` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `branch`
--
ALTER TABLE `branch`
  ADD CONSTRAINT `ffffffff` FOREIGN KEY (`ref_faculty`) REFERENCES `faculty` (`id_f`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `fffffffffffff` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id_f`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_employee_type_of_agencies1` FOREIGN KEY (`type_of_agencies_id`) REFERENCES `type_of_agencies` (`id_agen`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;